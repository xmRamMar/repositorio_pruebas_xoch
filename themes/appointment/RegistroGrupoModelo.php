<?php
/*
Template Name: Registro Convocatoria
*/ ?>

<!--campo en el cual se adicionan-->

<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
    <style>
        .customize-support>img {
            width: 100% !important;
        }
    </style>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php 
    $appointment_options=theme_setup_data(); 
    $header_setting = wp_parse_args(  get_option( 'appointment_options', array() ), $appointment_options);
    if($header_setting['upload_image_favicon']!=''){ ?>
    <link rel="shortcut icon" href="<?php  echo $header_setting['upload_image_favicon']; ?>" /> 
    <?php } ?>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <style>
        #fine-uploader-s3 .preview-link {
            display: block;
            height: 100%;
            width: 100%;
        }
    </style>
    <?php wp_head(); ?>
    <script src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/js/jquery.js" ></script>
    <script src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/js/spin.min.js" ></script>
    <!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
    <link href="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/css/fine-uploader.min.css" rel="stylesheet">

    <!-- Fine Uploader S3 JS file
    ====================================================================== -->
    <script src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/js/s3.fine-uploader.min.js"></script>
    
     <!-- Fine Uploader Customized Gallery template
    ====================================================================== -->
    <!--
    This is a legacy template and is not meant to be used in new Fine Uploader integrated projects.
    Read the "Getting Started Guide" at http://docs.fineuploader.com/quickstart/01-getting-started.html
    if you are not yet familiar with Fine Uploader UI.
-->
<script type="text/template" id="qq-template-s3">
    <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="Drop files here">
        <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
            <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
        </div>
        <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
            <span class="qq-upload-drop-area-text-selector"></span>
        </div>
        <div class="qq-upload-button-selector qq-upload-button">
            <div>Elegir Archivo</div>
        </div>
            
        <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
            <li>
                <div class="qq-progress-bar-container-selector">
                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                </div>
                
                <span class="qq-upload-file-selector qq-upload-file"></span>    
                <span class="qq-upload-size-selector qq-upload-size"></span>
                <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">Cancel</button>
                
                <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
            </li>
        </ul>

        <dialog class="qq-alert-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Close</button>
            </div>
        </dialog>

        <dialog class="qq-confirm-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">No</button>
                <button type="button" class="qq-ok-button-selector">Yes</button>
            </div>
        </dialog>

        <dialog class="qq-prompt-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <input type="text">
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Cancel</button>
                <button type="button" class="qq-ok-button-selector">Ok</button>
            </div>
        </dialog>
    </div>
</script>


</head>
    <body <?php body_class(); ?> >

<?php if ( get_header_image() != '') {?>
<div class="header-img">
    <div class="header-content">
        <?php if($header_setting['header_one_name'] != '') { ?>
        <h1><?php echo $header_setting['header_one_name'];?></h1>
        <?php }  if($header_setting['header_one_text'] != '') { ?>
        <h3><?php echo $header_setting['header_one_text'];?></h3>
        <?php } ?>
    </div>
    <img class="img-responsive" src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" />
</div>
<?php } ?>

<!--Logo & Menu Section-->  
<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
                <?php if($header_setting['text_title'] == 1) { ?>
                <h1><a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <?php
                     if($header_setting['enable_header_logo_text'] == 1) 
                    { echo "<div class=appointment_title_head>" . get_bloginfo( ). "</div>"; }
                    elseif($header_setting['upload_image_logo']!='') 
                    { ?>
                    <img class="img-responsive" src="../wp-content/themes/appointment/images/im/icons/Logo TI.png" style="height:<?php echo $header_setting['height']; ?>px; width:<?php echo $header_setting['width']; ?>px;"/>
                    <?php } else { ?>
                    <img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/logo.png">
                    <?php } ?>
                </a></h1>
                <?php } ?>  
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only"><?php _e('Toggle navigation','appointment'); ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        
        <?php 
                $facebook = $header_setting['social_media_facebook_link'];
                $twitter = $header_setting['social_media_twitter_link'];
                $linkdin = $header_setting['social_media_linkedin_link'];
                
                $social = '<ul id="%1$s" class="%2$s">%3$s';
                if($header_setting['header_social_media_enabled'] == 0 )
                {
                    $social .= '<ul class="head-contact-social">';

                    if($header_setting['social_media_facebook_link'] != '') {
                    $social .= '<li class="facebook"><a href="https://www.facebook.com/TI-929685520419250/"';
                        if($header_setting['facebook_media_enabled']==1)
                        {
                         $social .= 'target="_blank"';
                        }
                    $social .='><i class="fa fa-facebook"></i></a></li>';
                    }
                    if($header_setting['social_media_twitter_link']!='') {
                    $social .= '<li class="twitter"><a href="'.$twitter.'"';
                        if($header_setting['twitter_media_enabled']==1)
                        {
                         $social .= 'target="_blank"';
                        }
                    $social .='><i class="fa fa-twitter"></i></a></li>';
                    
                    }
                    if($header_setting['social_media_linkedin_link']!='') {
                    $social .= '<li class="linkedin"><a href="'.$linkdin.'"';
                        if($header_setting['linkedin_media_enabled']==1)
                        {
                         $social .= 'target="_blank"';
                        }
                    $social .='><i class="fa fa-linkedin"></i></a></li>';
                    }
                    $social .='</ul>'; 
                    
            }
            $social .='</ul>'; 
        
        ?>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <?php wp_nav_menu( array(  
                'theme_location' => 'primary',
                'container'  => '',
                'menu_class' => 'nav navbar-nav navbar-right',
                'fallback_cb' => 'webriti_fallback_page_menu',
                'items_wrap'  => $social,
                'walker' => new webriti_nav_walker()
                 ) );
                ?>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>  
<!--/Logo & Menu Section--> 
<div class="clearfix"></div>

<!--fin-->


<style>
   .wating{
    background: #FFFFFF;
    opacity: 0.5;
}
</style>
<a name="Nosotros" id="a"></a>

<!-- Modal -->
  <div class="modal" id="myModal" width="100%" height="100%" style="opacity:0.7; background:#ffffff;">
    <div class="waiting" id="myModalcontainer">
      
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <!-- Modal -->
  <div class="modal" id="modalSuccess" width="100%" height="100%" style="background-color:rgba(255,255,255,0.8);">
    <div class="waiting" id="modalSuccessContainer">
        <div class="container"><br/><br/></div>
        <div class="container jumbotron">
            <div class="container text-center">
              <h2 >Felicidades, te haz registrado exitósamente!!!</h2>
              <br/>
              <p>¿Planeas aterrizar tus idéas tecnológicas?... <strong>En HALER te podemos Ayudar!!!</strong>
              <br/>
              <br/>
              <div class="row">
                  <div class="col-md-6">
                    <div class="service-iconx">
                        <a target="_blank" href="http://www.haler.com.mx/the-engineers/">   
                        <img src="../wp-content/themes/appointment/images/TE.png" width="180px" alt="i">
                        </a>
                    </div>
                            <div class="media-body">
                                <p><a target="_blank" href="http://www.haler.com.mx/the-engineers/">Marketing Experience, Stands, BTL, AR, VR y más...</a></p>
                            </div>
                    </div>

                    <div class="col-md-6">
                    <div class="service-iconx">
                        <a target="_blank" href="http://www.haler.com.mx/ti/">
                        <img src="../wp-content/themes/appointment/images/TI.png" width="180px" alt="i">
                        </a>
                    </div>
                            <div class="media-body">
                                <p><a target="_blank" href="http://www.haler.com.mx/ti/">Software and Apps</a></p>
                            </div>
                    </div>
                </div>
                <h5 class="text-left" style="padding-top: 30px;"><strong><small><a href="http://www.haler.com.mx" target="_blank">* La plataforma de registro ha sido patrocinada por HALER Technological Development</a></small></strong></h5>
                <button type="button" value="Reload Page" onClick="clickCount();"> Entendido </button>
            </div>
        </div>
  
  </div>
</div>
        

        
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
<div class="container" >
    <div class="row" style="padding-top: 15px;">
        <div class="col-md-2">
            <a href="http://www.haler.com.mx/ti/">
            <img src="../wp-content/themes/appointment/images/TI.png" width="100px" alt="i">
            </a>
        </div>

        <div class="col-md-2">
            <a href="http://www.haler.com.mx/the-engineers/">
            <img src="../wp-content/themes/appointment/images/TE.png" width="100px" alt="i">
            </a>
        </div>      

        <div class="col-md-2">
        <a href="http://www.haler.com.mx/doin/">
            <img src="../wp-content/themes/appointment/images/DO.png" width="100px" alt="i">
            </a>
        </div>

        <div class="col-md-2">
            <a href="http://www.haler.com.mx/biomedic/">
            <img src="../wp-content/themes/appointment/images/BI.png" width="100px" alt="i">
            </a>
        </div>

        <div class="col-md-2">
            <a href="http://www.haler.com.mx/industry/">
            <img src="../wp-content/themes/appointment/images/IN.png" width="100px" alt="i">
            </a>
        </div>

        <div class="col-md-2">
            <a href="http://www.haler.com.mx/academy/">
            <img src="../wp-content/themes/appointment/images/AC.png" width="100px" alt="i">
            </a>
        </div>
    </div>
</div>
<!-- Blog Section -->
<div class="page-builder jumbotron">
    <div class="container">
        <div class="container">
        <h1  class="text-center" style="padding-top: 20px;">El Retail Del Futuro</h1>
        <h3  class="text-center" ><small>Registro</small></h3>
  <form id="theForm" role="form" data-toggle="validator">
  <legend class="col-form-legend">Datos del Participante</legend>
    <div class="form-group row has-feedback">
      <label for="inputName" class="col-sm-2 col-form-label">Nombre Completo</label>
      <div class="col-sm-10">
        <input type="text" maxlength="64" minlength="4" pattern="[a-zA-Z0-9 ñÑáÁéÉíÍóÓúÚ]+" class="form-control" id="inputName" placeholder="nombre y apellidos" required>
        <div class="help-block with-errors"></div>
      </div>
      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
    </div>

    <div class="form-group row has-feedback">
      <label for="inputTel" class="col-sm-2 col-form-label">Teléfono Contacto</label>
      <div class="col-sm-10">
        <input type="text" maxlength="20" minlength="6" pattern="[+a-z0-9 ]+" class="form-control" id="inputTel" placeholder="Lada y número" required>
      <div class="help-block with-errors"></div>
      </div>
      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
    </div>
    <div class="form-group row has-feedback">
      <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
      <div class="col-sm-10">
        <input type="email" maxlength="128" minlength="5" class="form-control" id="inputEmail3" placeholder="Email" required>
      <div class="help-block with-errors"></div>
      </div>
      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
    </div>
    <div class="form-group row has-feedback">
      <label for="inputCity" class="col-sm-2 col-form-label">Ciudad de Residencia</label>
      <div class="col-sm-10">
        <input type="text" maxlength="128" minlength="3" pattern="[a-zA-Z0-9 ñÑáÁéÉíÍóÓúÚ]+" class="form-control" id="inputCity" placeholder="Ciudad de Residencia" required>
      <div class="help-block with-errors"></div>
      </div>
      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
    </div>
    <br/>
    <fieldset class="form-group row has-feedback">
      <legend class="col-form-legend">Sobre la idea propuesta</legend>
          <div class="form-group row">
              <label for="inputProblemIdentification" class="col-form-label">Identificación de Problema</label>
              <textarea minlength="10" maxlength="1014" data-error="Parece que la idea aún está solo en tu cabeza... completa el campo!!!" class="form-control TEXTAREA" id="inputProblemIdentification" rows="3" placeholder="..." required></textarea>
              <div class="help-block with-errors"></div>
          </div>
        
        <div class="form-group row has-feedback">
              <label for="inputEnfoque" class="col-form-label">Enfoque de Propuesta</label>
                <textarea minlength="10" maxlength="1014" class="form-control" id="inputEnfoque" rows="3" placeholder="..." required></textarea>
                <div class="help-block with-errors"></div>
          </div>

          <div class="form-group row has-feedback">
              <label for="inputInv" class="col-form-label">Cuéntanos sobre ti</label>
                <textarea minlength="10" maxlength="1014" class="form-control textarea" id="inputInv" rows="3" placeholder="A qué te dedicas, cuales son tus hobbies" required></textarea>
                <div class="help-block with-errors"></div>
          </div>

           <div id="fine-uploader-s3"></div>
    </fieldset>

    <input type="submit" id="uploadAndRegister" class="btn" value="Registrar"></input>

  </form>
</div>

              <!-- Fine Uploader DOM Element
    ====================================================================== -->
   
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
    <!-- Your code to create an instance of Fine Uploader and bind to the DOM/template
    ====================================================================== -->
    <script>
        var s3Uploader;
        var opts = {
              lines: 13 // The number of lines to draw
            , length: 28 // The length of each line
            , width: 14 // The line thickness
            , radius: 42 // The radius of the inner circle
            , scale: 1 // Scales overall size of the spinner
            , corners: 1 // Corner roundness (0..1)
            , color: '#000' // #rgb or #rrggbb or array of colors
            , opacity: 0.25 // Opacity of the lines
            , rotate: 0 // The rotation offset
            , direction: 1 // 1: clockwise, -1: counterclockwise
            , speed: 1 // Rounds per second
            , trail: 60 // Afterglow percentage
            , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
            , zIndex: 2e9 // The z-index (defaults to 2000000000)
            , className: 'spinner' // The CSS class to assign to the spinner
            , top: '50%' // Top position relative to parent
            , left: '50%' // Left position relative to parent
            , shadow: false // Whether to render a shadow
            , hwaccel: false // Whether to use hardware acceleration
            , position: 'absolute' // Element positioning
        }
        var count = 0;
        $(document).ready(function() {
            
            console.log("-------- READY ---------");
            s3Uploader = new qq.s3.FineUploader({
            debug: true,
            multiple: false,
            element: document.getElementById('fine-uploader-s3'),
            template: 'qq-template-s3',
            autoUpload: false,
            request: {
                endpoint: "https://s3-us-west-2.amazonaws.com/convocatoria",
                accessKey: "AKIAJOUM5R4D5NANWPZQ",
            },
            signature: {
                endpoint: "<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/cors.php"
            },
            uploadSuccess: {
                endpoint: "<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/doyourstuff.php",
                method: 'POST'
            },
            iframeSupport: {
                localBlankPagePath: "<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/blank.html"
            },
            objectProperties: {
                acl: 'private',
                bucket: 'convocatoria',
                region: 'us-west-2'
            },
            cors: {
                expected: true
            },
            chunking: {
                enabled: true
            },
            resume: {
                enabled: true
            },
            validation: {
                acceptFiles: 'application/pdf,application/vnd.ms-powerpoint',
                allowedExtensions: ['pdf', 'ppt'],
                itemLimit: 5,
                sizeLimit: 15000000
            },
            thumbnails: {
                placeholders: {
                    notAvailablePath: "<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/placeholders/not_available-generic.png",
                    waitingPath: "<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/placeholders/waiting-generic.png"
                }
            },
            callbacks: {
                onComplete: function(id, name, response) {
                    // Hide modal
                    //alert("Hecho, tu registro se ha completado: "+id+ " " +name+ " "+JSON.stringify(response));
                    // Clear the fields
                    if(response.success) {
                        $('#inputName').val('');
                        $('#inputTel').val('');
                        $('#inputEmail3').val('');
                        $('#inputCity').val('');
                        $('#inputProblemIdentification').val('');
                        $('#inputInv').val('');
                        $('#inputEnfoque').val('');
                        // Show add modal
                        showAdModal();
                    }
                    // Hide wating modal
                    showModalWaiting(false);
                    
                    
                }
            }
        });
        console.log("---- Uploader setup... done -----");
        
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
            event.preventDefault();
            return false;
            }
        });

        $("#theForm").submit(function(event) {
            //console.log("SUBMITTING");
            event.preventDefault();
        });
        });
        
        showAdModal = function() {
            $('#modalSuccess').show();
        }
        //showAdModal();

        clickCount = function() {
            if (count > 1) {
                window.location.reload()
            } else {
                count++;
            }
        }
        showModalWaiting = function(option) {
            if (option) {
                $('#myModal').show();
                var target = document.getElementById('myModalcontainer');
                var spinner = new Spinner(opts).spin(target);
            } else {
                console.log('hidding');
                $('#myModal').hide(1000);
            }
        }
        $('#inputProblemIdentification').keyup(validateTextarea);
        $('#inputInv').keyup(validateTextarea);
        $('#inputEnfoque').keyup(validateTextarea);

        function validateTextarea() {
            var errorMsg = "NO SE DEBEN INTRODUCIR CARACTERES ESPECIALES COMO < >";
            var textarea = this;
            var pattern = new RegExp('^[a-zA-Z0-9 .,;:?¿!¡-ñÑáÁéÉíÍóÓúÚ]+$');
            // check each line of text
            $.each($(this).val().split("\n"), function () {
                // check if the line matches the pattern
                var hasError = !this.match(pattern);
                if (typeof textarea.setCustomValidity === 'function') {
                    textarea.setCustomValidity(hasError ? errorMsg : '');
                } else {
                    // Not supported by the browser, fallback to manual error display...
                    $(textarea).toggleClass('error', !!hasError);
                    $(textarea).toggleClass('ok', !hasError);
                    if (hasError) {
                        $(textarea).attr('title', errorMsg);
                    } else {
                        $(textarea).removeAttr('title');
                    }
                }
                return !hasError;
            });
        }

        qq(document.getElementById("uploadAndRegister")).attach('click', function() {
           // myForm.validate();
           showModalWaiting(true);
           //alert('Name filtered: ' + ($('#inputName').val()).replace('script', ''));
            $.ajax({
                method: 'POST',
                url: "<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/email_chek.php",
                data: {email: $('#inputEmail3').val()}
            }).done(function( datata ) {
                console.log(datata);
                if (datata === '{"result":\'ok\',"coincidencias":0}') {
                    // Checa que los campos estén completos
                    console.log(s3Uploader.getUploads());
                    s3Uploader.setUploadSuccessParams(
                        {   nombre: $('#inputName').val(), 
                            telefono: $('#inputTel').val(),
                            correo: $('#inputEmail3').val(),
                            ciudad: $('#inputCity').val(),
                            problematica: $('#inputProblemIdentification').val(),
                            investigacion: $('#inputInv').val(),
                            enfoque: $('#inputEnfoque').val(),
                            soluciones: '',
                            diseno: '',
                            medicion: '',
                            estrategia: '',
                            impacto: ''
                        });

                    try {
                        
                        if (s3Uploader.getUploads().length === 1) {
                            console.log("Subiendo archivo...");
                            s3Uploader.uploadStoredFiles();
                        } else {
                            console.log("No hay archivo seleccionado archivo...");
                            showModalWaiting(false);    
                        }
                    }
                    catch(err) {
                        showModalWaiting(false);
                    }
                    
                } else {
                    console.log('hidding modal');
                    showModalWaiting(false);
                    alert("El correo ya esta registrado en el sistema");
                }
            });
           
        });
    </script>
    
    </div>


</div>

<a name="Contacto" id="b"></a>
        <!--edición fondos-->
<?php 
$appointment_options=theme_setup_data();
$callout_setting = wp_parse_args(  get_option( 'appointment_options', array() ), $appointment_options );
if($callout_setting['home_call_out_area_enabled'] == 0 ) { 
 $imgURL = $callout_setting['callout_background'];
 if($imgURL != '') { ?>
<div class="callout-section" style="background-image:url('../wp-content/themes/appointment/images/im/Negro.png'); background-repeat: no-repeat; background-position: top left; background-attachment: fixed;">

<?php } 
else
{ ?> 
<div class="callout-section" style="background-color:#ccc;">
<?php } ?>
    <div class="overlay">
        <div class="container">
            <div class="row">   
                <div class="col-md-12"> 
                        
                        <h1><?php echo $callout_setting['home_call_out_title'];?></h1>
                         <p><?php echo $callout_setting['home_call_out_description']; ?></p>
                    
                        <div><?php if ( function_exists( 'smuzform_render_form' ) ) { smuzform_render_form('204'); }?></div>
                </div>  
            </div>          
        
        </div>
            
    </div>  
</div> 
<!-- /Callout Section -->
<div class="clearfix"></div>
<?php } ?>
<!--edición fondos-->
<style>
    #piebord > p{
        background-color: #fff!important;
        border-top: 5px #000 solid;
    }
</style>
<!-- /Blog Section with Sidebar -->

<?php get_footer(); ?>