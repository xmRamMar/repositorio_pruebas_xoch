<?php
/*
Template Name: foro meet-medic
*/ ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> >
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="../wp-content/themes/appointment/publish/publish/landing/assets/img/favicon.png" type="image/x-icon">
    <title>Foro Meet-Medic</title>
    <link rel="stylesheet" href="../wp-content/themes/appointment/publish/publish/landing/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../wp-content/themes/appointment/publish/publish/landing/assets/css/pe.css">
	<link rel="stylesheet" href="../wp-content/themes/appointment/dwqa-templates/assets/css/style.css">
    <link rel="stylesheet" href="../wp-content/themes/appointment/publish/publish/landing/assets/css/landing-page.min.css">
    <link rel="stylesheet" id="wpsm_faq-font-awesome-front-css" href="http://www.haler.com.mx/wp-content/plugins/faq-responsive/assets/css/font-awesome/css/font-awesome.min.css?ver=4.7.3" type="text/css" media="all">
    <link rel="stylesheet" id="wpsm_faq_bootstrap-front-css" href="http://www.haler.com.mx/wp-content/plugins/faq-responsive/assets/css/bootstrap-front.css?ver=4.7.3" type="text/css" media="all">
        <script type="text/javascript" src="http://www.haler.com.mx/wp-includes/js/jquery/jquery.js?ver=1.12.4"></script>
        <script type="text/javascript" src="http://www.haler.com.mx/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1"></script>
        <script type="text/javascript" src="http://www.haler.com.mx/wp-content/themes/appointment/js/bootstrap.min.js?ver=4.7.3"></script>
		<script type="text/javascript" src="../wp-content/themes/appointment/dwqa-templates/assets/js/dwqa-questions-list.js"></script>
    
    <link href="../wp-content/themes/appointment/publish/publish/landing/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
    <base target="_parent">
    <style>
        .list-group-item {
            border:0px;
        }
		
		.contenedor_principal{
			position:relative;
			margin: 5%;
			margin-top: 7%;
		}
		
		.forum_title{
			font-family: "Open Sans",sans-serif,tahoma;
			font-size: 35px;
			font-weight: 300;
			text-align: center;
		}
		
		.forum_title span {
			color: #2dc3e8;
			font-family: "Josefin Sans","Open Sans",sans-serif,tahoma;
			font-size: 45px;
		}
		
		.dwqa-question-category{
			pointer-events: none;
		}
		
    </style>
    </head>
<body class="landing-page">
    <div id="skrollr-body">
        <?php include 'header.html';?>
		<div class="contenedor_principal">
			<p class="forum_title">Bienvenidos/as al Foro de <span>Meet-Medic</span></p>
			<?php echo do_shortcode('[dwqa-list-questions]');?>
		</div>
        <?php include 'footer.html';?>
    </div>
</body>
    
</html>