		<?php 
		/**
		Template Name: Home Page
		*/
		
		get_header();
		//****** get index static banner  ********
		get_template_part('index', 'slider');
		
		//****** Orange Sidebar Area ********
		get_sidebar('orange');
				
		//****** get index service  ********				
		get_template_part('index', 'service');
		
		//****** get Home call out
		get_template_part('index','home-callout'); 	

		//****** get index News  ********
		get_template_part('index', 'news');        ?>


<!-- Blog Section -->
<div class="page-builder">
	<div class="container">
		<div class="row">
            			

			<!--Sidebar Area-->
		</div>
        <div class="row">
            <div class="col-md-6">
                <div class="service-iconx">
                            <img src="../wp-content/themes/appointment/images/im/icons/idea.png" width="100%"alt="i" >
                        </div>
                        <div class="media-body">
                            <h3>Misión</h3>
                        </div>
                        <div class="media-body">
                            <p>Ser una empresa que apoye a sus clientes al mejor desarrollo de sus productos, así como implementar nuevos sistemas de manufactura que incrementen la productividad, ganancias y calidad de sus productos mediante diseños que reduzcan tanto los costos como los tiempos de desarrollo e implementación.</p> 
                        </div>
            </div>
            
            <div class="col-md-6">
                <div class="service-iconx">
                    <img src="../wp-content/themes/appointment/images/im/icons/cohete.png" width="100%"alt="i" >
                </div>
                        <div class="media-body">
                            <h3>Visión</h3>
                        </div>
                        <div class="media-body">
                            <p>Ser la empresa líder en desarrollos tecnológicos que además de ser referente en el diseño, desarrollo de soluciones y servicios con altos estándares de calidad, ofrezca una experiencia agradable al usuario final.</p>
                        </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
         <div class="col-md-4">
                <div class="service-iconx">
                            <img src="../wp-content/themes/appointment/images/im/icons/trabajo.png" width="100%"alt="i" >
                        </div>
                        <div class="media-body">
                            <h3>Valores</h3>
                        </div>
                        <div class="media-body">
                            <p>Pasión<br>Responsabilidad<br>Calidad<br>Compromiso</p>
                            
                            
                            
                             
                        </div>
            </div>
            <div class="col-md-4"></div>
        </div>
	</div>
</div>
<?php 
				
		get_footer();
		
		?>