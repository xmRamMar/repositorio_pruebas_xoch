<?php get_header();?>
<div class="error-section">		
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="error-404">
					<div class="text-center"><i class="fa fa-chain-broken"></i></div>
					<h1><?php _e('Error 404','appointment'); ?></h1>
					<h4><?php _e('¡Lo sentimos!, la página solicitada no ha sido encontrada','appointment'); ?></h4>
                    <p><?php _e('Lamentamos mucho los inconvenientes generados.','appointment'); ?></p>
					<div class="error-btn-area"><a href="<?php echo esc_html(site_url());?>" class="error-btn"><?php _e('Go Back','appointment'); ?></a></div>
				</div>
			</div>
		</div>			
	</div>
</div>
<?php get_footer(); ?>