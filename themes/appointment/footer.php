<!-- Footer Section -->

<?php 

$appointment_options=theme_setup_data();
$footer_setting = wp_parse_args(  get_option( 'appointment_options', array() ), $appointment_options );
if ( is_active_sidebar( 'footer-widget-area' ) ) { ?>
<div class="footer-section">
	<div class="container">	
		<div class="row footer-widget-section">
			<?php  dynamic_sidebar( 'footer-widget-area' );	} ?>	
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- /Footer Section -->
<div class="clearfix"></div>



<div class="page-builder">
	<div class="container">
		<div class="row">
            <div class="col-md-3" align="left">
                <div class="pie">
                    <ul>
                        <h4>Empresa</h4>
                        <li style="list-style:none;"> <a href="#Nosotros"> ¿Quiénes somos? </a></li>
                        <li style="list-style:none;"> <a href="#Contacto"> Contacto </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3" align="left">
                <div class="pie">
                    <ul>
                        <h4>Ventas</h4>
                        <li style="list-style:none; ">  
                              <a href="tel:+525547546928"><i class="fa fa-phone-square" aria-hidden="true" title="(+52) 55 47 54 69 28"></i><b> Cd. Méx.</b> (+52) 55 47 54 69 28 </a>
                        </li>
                        <li style="list-style:none; ">  
                              <a href="tel:+522414179440"><i class="fa fa-phone-square" aria-hidden="true" title="(+52) 241 4179440"></i><b> Tlax.</b> (+52) 241 41 79 440 </a>
                        </li>

                        <li style="list-style:none; ">     
                              <a><i class="fa fa-envelope" aria-hidden="true" title="contacto@haler.com.mx"></i> contacto@haler.com.mx </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3" align="left">
                <div class="pie">
                    <ul>
                        <h4>Información</h4>
                        <li style="list-style:none;"> <a> Términos y condiciones </a></li>
                        <li style="list-style:none;"> <a href="../anuncio-de-privacidad/"> Anuncio de privacidad </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer Copyright Section -->
<div class="footer-copyright-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="footer-copyright">
					<?php if( $footer_setting['footer_menu_bar_enabled'] == 0) { ?>
					<?php echo $footer_setting[ 'footer_copyright_text']; ?>
					</a>
					<?php } // end if ?>
				</div>
			</div>
				<?php if($footer_setting['footer_social_media_enabled'] == 0 ) { 
			    $footer_facebook = $footer_setting['footer_social_media_facebook_link'];
				$footer_twitter = $footer_setting['footer_social_media_twitter_link'];
				$footer_linkdin = $footer_setting['footer_social_media_linkedin_link'];
				$footer_googleplus = $footer_setting['footer_social_media_googleplus_link'];
				$footer_skype = $footer_setting['footer_social_media_skype_link'];
				?>
			<div class="col-md-4">
			<ul class="footer-contact-social" hidden>
					<?php if($footer_setting['footer_social_media_facebook_link']!='') { ?>
					<li class="facebook"><a href="<?php echo esc_url($footer_facebook); ?>" <?php if($footer_setting['footer_facebook_media_enabled']==1){ echo "target='_blank'"; } ?> ><i class="fa fa-facebook"></i></a></li>
					<?php } if($footer_setting['footer_social_media_twitter_link']!='') { ?>
					<li class="twitter"><a href="<?php echo esc_url($footer_twitter); ?>" <?php if($footer_setting['footer_twitter_media_enabled']==1){ echo "target='_blank'"; } ?> ><i class="fa fa-twitter"></i></a></li>
					
					<?php } ?>
				</ul>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
<!-- /Footer Copyright Section -->
<!--Scroll To Top--> 
<a href="#" class="hc_scrollup"><i class="fa fa-chevron-up"></i></a>
<!--/Scroll To Top--> 
<?php wp_footer(); ?>
<div id="piebord">
<p style="text-align: center; background-color: #80aa22;color: #000!important; padding:2%;">
<i class="fa fa-map-marker" aria-hidden="true" style="font-size: xx-large;"></i></br><b>Matriz: </b>Av. Felipe Lardizabal, No. 2719 A. La Cañada C.P. 90360 Apizaco, Tlaxcala.</br><b>Sucursal: </b>Av. Amacuzac, No. 209. Santiago Sur. C.P. 08800. Iztacalco, Ciudad de México.</p>

</div>
</body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-85593493-2', 'auto');
  ga('send', 'pageview');

</script>
</html>