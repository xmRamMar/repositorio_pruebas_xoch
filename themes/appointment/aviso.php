<?php
/*
Template Name: Aviso de Privacidad
*/ ?>

<?php
get_header(); ?>

<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Constantia;
	panose-1:2 3 6 2 5 3 6 3 3 3;
	mso-font-charset:0;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611985 1073750091 0 0 415 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:0cm;
	line-height:115%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Constantia","serif";
	mso-ascii-font-family:Constantia;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Constantia;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Constantia;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;
	mso-ansi-language:ES;
	mso-fareast-language:EN-US;}
h2
	{mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-link:"Título 2 Car";
	mso-style-next:Normal;
	margin-top:10.0pt;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:0cm;
	margin-bottom:.0001pt;
	line-height:115%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:2;
	font-size:13.0pt;
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:LiSu;
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#0F6FC6;
	mso-themecolor:accent1;
	mso-ansi-language:ES;
	mso-fareast-language:EN-US;
	font-weight:bold;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-priority:99;
	mso-style-link:"Encabezado Car";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 220.95pt right 441.9pt;
	font-size:11.0pt;
	font-family:"Constantia","serif";
	mso-ascii-font-family:Constantia;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Constantia;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Constantia;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;
	mso-ansi-language:ES;
	mso-fareast-language:EN-US;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-priority:99;
	mso-style-link:"Pie de página Car";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 220.95pt right 441.9pt;
	font-size:11.0pt;
	font-family:"Constantia","serif";
	mso-ascii-font-family:Constantia;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Constantia;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Constantia;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;
	mso-ansi-language:ES;
	mso-fareast-language:EN-US;}

a:visited, span.MsoHyperlinkFollowed
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:#85DFD0;
	mso-themecolor:followedhyperlink;
	text-decoration:underline;
	text-underline:single;}
p
	{mso-style-priority:99;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";
	mso-ansi-language:ES;
	mso-fareast-language:ES;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{mso-style-priority:34;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:36.0pt;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Constantia","serif";
	mso-ascii-font-family:Constantia;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Constantia;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Constantia;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;
	mso-ansi-language:ES;
	mso-fareast-language:EN-US;}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{mso-style-priority:34;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-type:export-only;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Constantia","serif";
	mso-ascii-font-family:Constantia;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Constantia;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Constantia;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;
	mso-ansi-language:ES;
	mso-fareast-language:EN-US;}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{mso-style-priority:34;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-type:export-only;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Constantia","serif";
	mso-ascii-font-family:Constantia;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Constantia;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Constantia;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;
	mso-ansi-language:ES;
	mso-fareast-language:EN-US;}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{mso-style-priority:34;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-type:export-only;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:36.0pt;
	mso-add-space:auto;
	line-height:115%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Constantia","serif";
	mso-ascii-font-family:Constantia;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Constantia;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Constantia;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;
	mso-ansi-language:ES;
	mso-fareast-language:EN-US;}
span.apple-converted-space
	{mso-style-name:apple-converted-space;
	mso-style-unhide:no;}
span.EncabezadoCar
	{mso-style-name:"Encabezado Car";
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:Encabezado;}
span.PiedepginaCar
	{mso-style-name:"Pie de página Car";
	mso-style-priority:99;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Pie de página";}
span.Ttulo2Car
	{mso-style-name:"Título 2 Car";
	mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Título 2";
	mso-ansi-font-size:13.0pt;
	mso-bidi-font-size:13.0pt;
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:LiSu;
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#0F6FC6;
	mso-themecolor:accent1;
	font-weight:bold;}
span.SpellE
	{mso-style-name:"";
	mso-spl-e:yes;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-family:"Constantia","serif";
	mso-ascii-font-family:Constantia;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:Constantia;
	mso-fareast-theme-font:minor-latin;
	mso-hansi-font-family:Constantia;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;
	mso-ansi-language:ES;
	mso-fareast-language:EN-US;}
.MsoPapDefault
	{mso-style-type:export-only;
	margin-bottom:10.0pt;
	line-height:115%;}
    body span {
    display: inline !important;
    }
 /* Page Definitions */
 @page
	{mso-footnote-separator:url("AVISO%20DE%20PRIVACIDAD%20INTEGRAL%20PARA%20CLIENTES%20Y%20PROVEEDORES%20-%20HALER_archivos/header.htm") fs;
	mso-footnote-continuation-separator:url("AVISO%20DE%20PRIVACIDAD%20INTEGRAL%20PARA%20CLIENTES%20Y%20PROVEEDORES%20-%20HALER_archivos/header.htm") fcs;
	mso-endnote-separator:url("AVISO%20DE%20PRIVACIDAD%20INTEGRAL%20PARA%20CLIENTES%20Y%20PROVEEDORES%20-%20HALER_archivos/header.htm") es;
	mso-endnote-continuation-separator:url("AVISO%20DE%20PRIVACIDAD%20INTEGRAL%20PARA%20CLIENTES%20Y%20PROVEEDORES%20-%20HALER_archivos/header.htm") ecs;}
@page WordSection1
	{size:612.1pt 792.1pt;
	margin:70.85pt 3.0cm 70.85pt 3.0cm;
	mso-header-margin:34.0pt;
	mso-footer-margin:65.2pt;
	mso-footer:url("AVISO%20DE%20PRIVACIDAD%20INTEGRAL%20PARA%20CLIENTES%20Y%20PROVEEDORES%20-%20HALER_archivos/header.htm") f1;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 @list l0
	{mso-list-id:259800979;
	mso-list-type:hybrid;
	mso-list-template-ids:-462644266 131134502 201981977 201981979 201981967 201981977 201981979 201981967 201981977 201981979;}
@list l0:level1
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-weight:bold;}
@list l0:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l0:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l0:level4
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l0:level5
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l0:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l0:level7
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l0:level8
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l0:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l1
	{mso-list-id:449130353;
	mso-list-type:hybrid;
	mso-list-template-ids:-593609644 -244945884 201981977 201981979 201981967 201981977 201981979 201981967 201981977 201981979;}
@list l1:level1
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-weight:bold;}
@list l1:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l1:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l1:level4
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l1:level5
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l1:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l1:level7
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l1:level8
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l1:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l2
	{mso-list-id:518465685;
	mso-list-type:hybrid;
	mso-list-template-ids:-1862879256 -826507518 201981977 201981979 201981967 201981977 201981979 201981967 201981977 201981979;}
@list l2:level1
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-weight:bold;}
@list l2:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l2:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l2:level4
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l2:level5
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l2:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l2:level7
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l2:level8
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l2:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l3
	{mso-list-id:610237843;
	mso-list-type:hybrid;
	mso-list-template-ids:1666757560 201981967 201981977 201981979 201981967 201981977 201981979 201981967 201981977 201981979;}
@list l3:level1
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l3:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l3:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l3:level4
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l3:level5
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l3:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l3:level7
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l3:level8
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l3:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l4
	{mso-list-id:1282804330;
	mso-list-type:hybrid;
	mso-list-template-ids:1666757560 201981967 201981977 201981979 201981967 201981977 201981979 201981967 201981977 201981979;}
@list l4:level1
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l4:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l4:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l4:level4
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l4:level5
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l4:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l4:level7
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l4:level8
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l4:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l5
	{mso-list-id:1778865276;
	mso-list-type:hybrid;
	mso-list-template-ids:-50538784 125054484 201981977 201981979 201981967 201981977 201981979 201981967 201981977 201981979;}
@list l5:level1
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-weight:bold;}
@list l5:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l5:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l5:level4
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l5:level5
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l5:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l5:level7
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l5:level8
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l5:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l6
	{mso-list-id:1806464662;
	mso-list-type:hybrid;
	mso-list-template-ids:-462644266 131134502 201981977 201981979 201981967 201981977 201981979 201981967 201981977 201981979;}
@list l6:level1
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-weight:bold;}
@list l6:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l6:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l6:level4
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l6:level5
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l6:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l6:level7
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l6:level8
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l6:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>
<div class="page-builder">
	<div class="container">
<div class=WordSection1 >

<p style='margin:0cm;margin-bottom:.0001pt;line-height:15.0pt;vertical-align:
baseline'><b><span lang=ES style='font-family:"Arial","sans-serif"'>AVISO DE
PRIVACIDAD.<o:p></o:p></span></b></p>

<p align=center style='margin:0cm;margin-bottom:.0001pt;text-align:center;
line-height:15.0pt;vertical-align:baseline'><span lang=ES style='font-family:
"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'>HALER
TECHNOLOGICAL DEVELOPMENT, S.A. DE C.V. (en adelante, <b style='mso-bidi-font-weight:
normal'>“HALER”</b>), con domicilio ubicado en Amacuzac número 209, col.
Santiago Sur, C.P. 08800, delegación Iztacalco, México, Distrito Federal, es
responsable de recabar los datos personales que usted le proporcione por
cualquiera de los medios previstos en este Aviso de Privacidad, así como del
uso que se le dé a los mismos y de su protección.<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:15.0pt;margin-left:
0cm;text-align:justify;line-height:15.0pt;vertical-align:baseline'><i
style='mso-bidi-font-style:normal'><span lang=ES style='font-family:"Arial","sans-serif"'>DATOS
PERSONALES QUE PUEDEN SER RECABADOS.<o:p></o:p></span></i></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:15.0pt;margin-left:
0cm;text-align:justify;line-height:15.0pt;vertical-align:baseline'><span
lang=ES style='font-family:"Arial","sans-serif"'>Para las finalidades antes
mencionadas, <b style='mso-bidi-font-weight:normal'>HALER</b> requiere obtener
los siguientes datos personales de sus clientes, representantes de sus clientes,
de sus proveedores o de los representantes legales de sus proveedores: nombre
completo, género, fecha y lugar de nacimiento, edad, domicilio, domicilio
fiscal, estado civil, RFC, CURP, número de seguridad social, cuentas bancarias
o de tarjetas de crédito, huellas dactilares y/o firma autógrafa, correo
electrónico, número telefónico local y de celular, usuario de facebook, twitter y linkedin, nivel
de escolaridad, nivel de ingresos, página web, nombre de la empresa en la que
trabaja, área, cargo que tiene asignado en la empresa que trabaja, nombre de
sus familiares en línea recta, datos de localización, datos patrimoniales, datos
financieros, datos corporativos, firma electrónica avanzada y certificado
digital emitido por el Servicio de Administración Tributaria y los que en su
momento se consideren necesarios para llevar a cabo una adecuada relación con <b
style='mso-bidi-font-weight:normal'>HALER</b>, sin que estos sean considerados
como sensibles. Los datos personales serán conservados por el tiempo que sea
necesario para cumplir con las finalidades descritas en este Aviso de
Privacidad, o por un tiempo menor siempre que sea solicitada por escrito la
cancelación de sus datos personales en los términos del presente Aviso de
Privacidad.<o:p></o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:15.0pt;margin-left:
0cm;text-align:justify;line-height:15.0pt;vertical-align:baseline'><span
lang=ES style='font-family:"Arial","sans-serif"'>En caso de que en su momento
se requiera recabar datos sensibles, según lo establecido en la Ley Federal de
Protección de Datos Personales en Posesión de los Particulares (en adelante, la
“Ley”), nos podrá proporcionar alguno o todos los datos referentes a: origen
racial o étnico, estado de salud, información genética, creencias religiosas,
filosóficas y morales, afiliación sindical, opiniones políticas, preferencia
sexual, relaciones personales o cualquier otro dato personal que pueda
considerarse sensible.<o:p></o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:15.0pt;margin-left:
0cm;text-align:justify;line-height:15.0pt;vertical-align:baseline'><span
lang=ES style='font-family:"Arial","sans-serif"'>Por tal motivo, si le llega a
proporcionar a <b style='mso-bidi-font-weight:normal'>HALER </b>datos
personales sensibles, datos financieros y patrimoniales, antes de proporcionarlos
o en el momento de la entrega, deberá proporcionar de forma expresa, por
escrito o verbalmente, su consentimiento para que <b style='mso-bidi-font-weight:
normal'>HALER</b> tenga y maneje dicha información, ya que de lo contrario <b
style='mso-bidi-font-weight:normal'>HALER</b> no se encontrará en condiciones
de proporcionar sus productos, prestar los servicios que le hayan sido
contratados o realizar la compra de los productos o servicios que el proveedor
le preste o le desee prestar.<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><i style='mso-bidi-font-style:normal'><span lang=ES
style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></i></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><i style='mso-bidi-font-style:normal'><span lang=ES
style='font-family:"Arial","sans-serif"'>FINALIDADES EN LAS QUE SE UTILIZARÁN
LOS DATOS PERSONALES.<o:p></o:p></span></i></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><u><span lang=ES style='font-family:"Arial","sans-serif"'><o:p><span
 style='text-decoration:none'>&nbsp;</span></o:p></span></u></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'>Salvo
que el titular disponga lo contrario, mediante la forma descrita en el presente
Aviso de Privacidad, los datos personales serán utilizados para las siguientes
finalidades:<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><b style='mso-bidi-font-weight:normal'><span lang=ES
style='font-family:"Arial","sans-serif"'>A)</span></b><span lang=ES
style='font-family:"Arial","sans-serif"'> Necesarias para la relación entre <b
style='mso-bidi-font-weight:normal'>HALER</b> y el titular de los datos
personales.<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36.0pt;
margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;line-height:15.0pt;
mso-list:l5 level1 lfo1;vertical-align:baseline'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=ES style='font-family:"Arial","sans-serif";
mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><span
lang=ES style='font-family:"Arial","sans-serif"'>Otorgar los productos y servicios
que le sean solicitados<b> </b><span style='mso-bidi-font-weight:bold'>por sus
clientes</span>, así como para la realización de actividades afines y de
aquellas que sean necesarias para el complemento de los servicios solicitados;<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36.0pt;
margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;line-height:15.0pt;
mso-list:l5 level1 lfo1;vertical-align:baseline'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=ES style='font-family:"Arial","sans-serif";
mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><span
lang=ES style='font-family:"Arial","sans-serif"'>Contratar los productos y
servicios de los proveedores cuando así lo requiera <b style='mso-bidi-font-weight:
normal'>HALER</b>;<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36.0pt;
margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;line-height:15.0pt;
mso-list:l5 level1 lfo1;tab-stops:297.7pt;vertical-align:baseline'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=ES style='font-family:"Arial","sans-serif";
mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><span
lang=ES style='font-family:"Arial","sans-serif"'>Dar cumplimiento a
obligaciones contraídas con los clientes, proveedores o alianzas;<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36.0pt;
margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;line-height:15.0pt;
mso-list:l5 level1 lfo1;vertical-align:baseline'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=ES style='font-family:"Arial","sans-serif";
mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><span
lang=ES style='font-family:"Arial","sans-serif"'>Realizar trámites, informar y
dar avisos ante instituciones, autoridades, organismos públicos, relacionados
con los clientes, con la finalidad de dar cumplimiento a las actividades
inherentes a la prestación de servicios que realiza <b style='mso-bidi-font-weight:
normal'>HALER</b> con sus clientes y para el cumplimiento de obligaciones o
requerimientos de autoridades;<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36.0pt;
margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;line-height:15.0pt;
mso-list:l5 level1 lfo1;vertical-align:baseline'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=ES style='font-family:"Arial","sans-serif";
mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><span
lang=ES style='font-family:"Arial","sans-serif"'>Solicitar a sus clientes,
proveedores, o alianzas, cumplan con sus obligaciones y en su caso con el pago
de los productos y servicios que les haya otorgado, así como para hacer
exigibles dichas obligaciones ante cualquier medio e instancia legal que <b
style='mso-bidi-font-weight:normal'>HALER</b> crea conveniente y le corresponda
conforme a derecho; y<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36.0pt;
margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;line-height:15.0pt;
mso-list:l5 level1 lfo1;vertical-align:baseline'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=ES style='font-family:"Arial","sans-serif";
mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>6.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><span
lang=ES style='font-family:"Arial","sans-serif"'>Dar aviso respecto de,
cualquier modificación a las condiciones en las que se proveen o contratan los
productos o servicios, los requerimientos de información de los datos personales
del titular<b style='mso-bidi-font-weight:normal'> </b>que lleguen a ser<b
style='mso-bidi-font-weight:normal'> </b>realizados por cualquier autoridad
local o federal, institución u organismo público, que cuente con la facultad de
realizar dicha solicitud, con la finalidad de que el titular de los datos
personales le otorgue a<b style='mso-bidi-font-weight:normal'> HALER</b> su
autorización para transmitir dicha información, siempre y cuando dicho titular
cuente con la facultad de decidir proporcionar o no su información, ya que en
caso de que la legislación aplicable establezca como obligatorio que<b
style='mso-bidi-font-weight:normal'> HALER</b> deba proporcionar los datos
personales del titular, <b style='mso-bidi-font-weight:normal'>HALER </b>sólo
le informará sobre la información que proporcionará o haya proporcionado a la
autoridad, institución u organismo público que lo haya solicitado, siempre que
cuente con la facultad para solicitar dicha información.<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><b style='mso-bidi-font-weight:normal'><span lang=ES
style='font-family:"Arial","sans-serif"'>B)</span></b><span lang=ES
style='font-family:"Arial","sans-serif"'> Secundarias para la relación entre <b
style='mso-bidi-font-weight:normal'>HALER </b>y el titular de los datos
personales.<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36.0pt;
margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;line-height:15.0pt;
mso-list:l1 level1 lfo4;vertical-align:baseline'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=ES style='font-family:"Arial","sans-serif";
mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><span
lang=ES style='font-family:"Arial","sans-serif"'>La evaluación de la calidad
del producto y servicio prestado;<o:p></o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36.0pt;
margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;vertical-align:
baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36.0pt;
margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;line-height:15.0pt;
mso-list:l1 level1 lfo4;vertical-align:baseline'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=ES style='font-family:"Arial","sans-serif";
mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><span
lang=ES style='font-family:"Arial","sans-serif"'>Enviar invitaciones e
información relevante que pudieran ser de interés del titular de los datos
personales; <o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36.0pt;
margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;line-height:15.0pt;
mso-list:l1 level1 lfo4;vertical-align:baseline'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=ES style='font-family:"Arial","sans-serif";
mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><span
lang=ES style='font-family:"Arial","sans-serif"'>Dar a conocer promociones y
nuevos productos y servicios;<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36.0pt;
margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;line-height:15.0pt;
mso-list:l1 level1 lfo4;vertical-align:baseline'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=ES style='font-family:"Arial","sans-serif";
mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;</span></span></span></b><![endif]><span
lang=ES style='font-family:"Arial","sans-serif"'>Proporcionar publicidad,
mercadotecnia o actividades de prospección comercial, relacionada únicamente
con los productos y servicios que ofrece <b style='mso-bidi-font-weight:normal'>HALER</b>;
y<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36.0pt;
margin-bottom:.0001pt;text-align:justify;text-indent:-18.0pt;line-height:15.0pt;
mso-list:l1 level1 lfo4;vertical-align:baseline'><![if !supportLists]><b
style='mso-bidi-font-weight:normal'><span lang=ES style='font-family:"Arial","sans-serif";
mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span></b><![endif]><span
lang=ES style='font-family:"Arial","sans-serif"'>Dar aviso de cambios al
presente Aviso de Privacidad.<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><u><span lang=ES style='font-family:"Arial","sans-serif"'><o:p><span
 style='text-decoration:none'>&nbsp;</span></o:p></span></u></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><i style='mso-bidi-font-style:normal'><span lang=ES
style='font-family:"Arial","sans-serif"'>NEGATIVA DEL TRATAMIENTO DE DATOS
PERSONALES.<o:p></o:p></span></i></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><u><span lang=ES style='font-family:"Arial","sans-serif"'><o:p><span
 style='text-decoration:none'>&nbsp;</span></o:p></span></u></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'>Si
el titular desea manifestar su negativa al tratamiento de sus datos personales
a las finalidades secundarias mencionadas en el inciso <b style='mso-bidi-font-weight:
normal'>B)</b> anterior, podrá solicitarlo por escrito con acuse de recibo a<span
class=apple-converted-space>&nbsp;</span><b>HALER</b><span
class=apple-converted-space>&nbsp;</span>al correo electrónico, en caso de que
pueda firmarlo mediante firma electrónica, o al domicilio señalados en este
Aviso de Privacidad, señalando domicilio y correo electrónico para recibir
notificaciones derivadas de dicha petición. <o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'>Cuando
el presente Aviso de Privacidad&nbsp;no se haga del conocimiento del titular de
manera directa, el titular tiene un plazo de cinco días hábiles para que, de
ser el&nbsp;caso, manifieste su negativa para el tratamiento de sus datos
personales.<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><u><span lang=ES style='font-family:"Arial","sans-serif"'><o:p><span
 style='text-decoration:none'>&nbsp;</span></o:p></span></u></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><u><span lang=ES style='font-family:"Arial","sans-serif"'><o:p><span
 style='text-decoration:none'>&nbsp;</span></o:p></span></u></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><i style='mso-bidi-font-style:normal'><span lang=ES
style='font-family:"Arial","sans-serif";mso-bidi-font-weight:bold'>TRANSFERENCIA
DE DATOS PERSONALES.<o:p></o:p></span></i></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><b><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><b><span lang=ES style='font-family:"Arial","sans-serif"'>HALER</span></b><span
class=apple-converted-space><span lang=ES style='font-family:"Arial","sans-serif"'>&nbsp;</span></span><span
lang=ES style='font-family:"Arial","sans-serif"'>podrá compartir los datos que
recabe de los titulares de datos personales con sus empleados o colaboradores, proveedores,
alianzas comerciales, clientes o cualquier otra persona que se involucre con
las finalidades necesarias mencionadas en el inciso <b style='mso-bidi-font-weight:
normal'>A)</b> anterior, estando obligados éstos en los mismos términos y
condiciones que<span class=apple-converted-space>&nbsp;</span><b>HALER </b>respecto
al manejo de los mismos, para lo cual se les mostrará el presente Aviso de
Privacidad para que conozcan los términos en los que se obligan. En ningún caso
los datos personales serán transmitidos a terceras personas fuera de los casos
establecidos en el presente Aviso de Privacidad o en la Ley, y cuando por
cualquier motivo se requiera realizar la transferencia de datos a terceros no
contemplados en el presente Aviso de Privacidad, le será solicitada
autorización al titular, previa a la transmisión de sus datos personales. <o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'>Asimismo,
informamos que sus datos personales pueden ser transferidos y tratados dentro y
fuera del país, por personas distintas a <b style='mso-bidi-font-weight:normal'>HALER</b>.
En ese sentido, su información puede ser compartida con otras sociedades o
personas que presten o contraten servicios o se encarguen de la
comercialización de productos o servicios relacionados con los servicios o
productos que <b style='mso-bidi-font-weight:normal'>HALER </b>le presta o
contrata, lo cual podrá realizarse con la finalidad de brindarle o contratarle los
productos o servicios que le ha contratado o prestado a <b style='mso-bidi-font-weight:
normal'>HALER</b>. Adicionalmente, la información puede ser compartida con diversas
entidades públicas o privadas para la prestación o contratación de los productos
o servicios solicitados, para solicitarle el cumplimiento del pago o de las
obligaciones contraídas por los productos o servicios prestados o contratados
por <b style='mso-bidi-font-weight:normal'>HALER</b>, o para el cumplimiento de
las obligaciones legales. <o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'>Además
de lo mencionado anteriormente, se le informa que sus datos personales podrán
ser proporcionados a la Secretaría de Hacienda y Crédito Público por medio de
un aviso, sin hacerlo de su conocimiento, siempre que se detecte una actividad
vulnerable de acuerdo a lo establecido por la Ley Federal para la Prevención e
Identificación de Operaciones con Recursos de Procedencia Ilícita, al momento
de que <b style='mso-bidi-font-weight:normal'>HALER</b> se encuentre
prestándole sus servicios. <o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'>Si
no se manifiesta oposición expresa y documentada por el titular de los datos
personales,&nbsp;o por su representante legal en los términos previstos en este
Aviso de Privacidad, para que sus datos personales sean transferidos, se
entenderá que ha otorgado su consentimiento para ello.<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><i style='mso-bidi-font-style:normal'><span lang=ES
style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></i></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><i style='mso-bidi-font-style:normal'><span lang=ES
style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></i></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><i style='mso-bidi-font-style:normal'><span lang=ES
style='font-family:"Arial","sans-serif"'>DERECHOS DE ACCESO, RECTIFICACIÓN,
CANCELACIÓN, OPOSICIÓN (DERECHOS ARCO) O LA REVOCACIÓN DEL CONSENTIMIENTO.<o:p></o:p></span></i></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><u><span lang=ES style='font-family:"Arial","sans-serif"'><o:p><span
 style='text-decoration:none'>&nbsp;</span></o:p></span></u></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:15.0pt;margin-left:
0cm;text-align:justify;line-height:15.0pt;vertical-align:baseline'><span
lang=ES style='font-family:"Arial","sans-serif"'>En cualquier momento y de
forma gratuita usted tiene el derecho de <u>acceder</u> a sus datos personales
que poseemos y a los detalles del tratamiento de los mismos, así como a <u>rectificarlos</u>
en caso de ser inexactos o incompletos, instruirnos a <u>cancelarlos</u> cuando
considere que resulten ser excesivos o innecesarios para las finalidades que
justificaron su obtención u <u>oponerse</u> al tratamiento de los mismos para
fines específicos (en adelante, “LOS DERECHOS ARCO”), o revocar su
consentimiento para que <b style='mso-bidi-font-weight:normal'>HALER</b> pueda
usar sus datos personales, los cuales siempre deberán ser ejercidos conforme a
la Ley. <o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'>Para
ejercer sus derechos ARCO o revocar su consentimiento para el tratamiento de
los datos personales, el titular de los mismos deberá presentar su solicitud en
nuestro domicilio ubicado en Amacuzac número 209, col. Santiago Sur, C.P.
08800, delegación Iztacalco, México, Distrito Federal, o enviarla a nuestro
departamento de datos personales al correo electrónico:<b style='mso-bidi-font-weight:normal'>direccion@haler.com.mx</b>&nbsp;con
atención al departamento de datos personales. Dicha solicitud deberá cumplir
con los siguientes requisitos:<o:p></o:p></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:15.0pt;margin-left:
36.0pt;text-align:justify;text-indent:-18.0pt;line-height:15.0pt;mso-list:l6 level1 lfo6;
vertical-align:baseline'><![if !supportLists]><b style='mso-bidi-font-weight:
normal'><span lang=ES style='font-family:"Arial","sans-serif";mso-fareast-font-family:
Arial'><span style='mso-list:Ignore'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=ES style='font-family:"Arial","sans-serif"'>Solicitud
original, en idioma español, dirigirla al departamento de datos personales de <b
style='mso-bidi-font-weight:normal'>HALER</b>, que contenga nombre completo del
titular de los datos personales, y el medio para que <b style='mso-bidi-font-weight:
normal'>HALER</b> se comunique con el titular;<o:p></o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:15.0pt;margin-left:
36.0pt;text-align:justify;text-indent:-18.0pt;line-height:15.0pt;mso-list:l6 level1 lfo6;
vertical-align:baseline'><![if !supportLists]><b style='mso-bidi-font-weight:
normal'><span lang=ES style='font-family:"Arial","sans-serif";mso-fareast-font-family:
Arial'><span style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=ES style='font-family:"Arial","sans-serif"'>La
solicitud deberá estar firmada por el titular de los datos personales o por su
representante, en caso de que la solicitud sea enviada por correo electrónico,
la firma deberá ser electrónica avanzada emitida por el Servicio de
Administración Tributaria;<o:p></o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:15.0pt;margin-left:
36.0pt;text-align:justify;text-indent:-18.0pt;line-height:15.0pt;mso-list:l6 level1 lfo6;
vertical-align:baseline'><![if !supportLists]><b style='mso-bidi-font-weight:
normal'><span lang=ES style='font-family:"Arial","sans-serif";mso-fareast-font-family:
Arial'><span style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=ES style='font-family:"Arial","sans-serif"'>En
caso de que la solicitud sea firmada por su representante, se deberá presentar
escritura pública o carta poder con la que acredite la facultad que le otorgó
el titular de los datos personales, la carta poder deberá estar firmada de
forma autógrafa por el titular de los datos personales y por dos testigos. En
caso de que la solicitud sea presentada de forma electrónica será suficiente
que la escritura pública o carta poder original sea escaneada y enviada junto
con la solicitud;<o:p></o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:15.0pt;margin-left:
36.0pt;text-align:justify;text-indent:-18.0pt;line-height:15.0pt;mso-list:l6 level1 lfo6;
vertical-align:baseline'><![if !supportLists]><b style='mso-bidi-font-weight:
normal'><span lang=ES style='font-family:"Arial","sans-serif";mso-fareast-font-family:
Arial'><span style='mso-list:Ignore'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=ES style='font-family:"Arial","sans-serif"'>Copia
simple de una identificación oficial vigente del titular de los datos
personales, de su representante, en caso de contar con él, y de los dos
testigos, siempre que presente carta poder;<o:p></o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:15.0pt;margin-left:
36.0pt;text-align:justify;text-indent:-18.0pt;line-height:15.0pt;mso-list:l6 level1 lfo6;
vertical-align:baseline'><![if !supportLists]><b style='mso-bidi-font-weight:
normal'><span lang=ES style='font-family:"Arial","sans-serif";mso-fareast-font-family:
Arial'><span style='mso-list:Ignore'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=ES style='font-family:"Arial","sans-serif"'>El
derecho que desea ejercer; <o:p></o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:15.0pt;margin-left:
36.0pt;text-align:justify;text-indent:-18.0pt;line-height:15.0pt;mso-list:l6 level1 lfo6;
vertical-align:baseline'><![if !supportLists]><b style='mso-bidi-font-weight:
normal'><span lang=ES style='font-family:"Arial","sans-serif";mso-fareast-font-family:
Arial'><span style='mso-list:Ignore'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=ES style='font-family:"Arial","sans-serif"'>Una
descripción clara y precisa de los datos personales respecto de los cuáles
busca ejercer su derecho; y<o:p></o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:15.0pt;margin-left:
36.0pt;text-align:justify;text-indent:-18.0pt;line-height:15.0pt;mso-list:l6 level1 lfo6;
vertical-align:baseline'><![if !supportLists]><b style='mso-bidi-font-weight:
normal'><span lang=ES style='font-family:"Arial","sans-serif";mso-fareast-font-family:
Arial'><span style='mso-list:Ignore'>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><span lang=ES style='font-family:"Arial","sans-serif"'>Cualquier
otro documento que considere facilite la localización de los datos.<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'>El
tiempo de respuesta que tendrá <b style='mso-bidi-font-weight:normal'>HALER</b>
a la solicitud del titular de los datos personales será de 20 días hábiles
contados desde la fecha en que se recibió y acuso de recibido la solicitud.<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'>Para
el caso de que el titular de los datos personales no presente su solicitud de
forma completa o clara, <b style='mso-bidi-font-weight:normal'>HALER</b> se lo
informará al titular y le solicitará que cumpla con la información faltante, lo
cual podrá realizar dentro de un tiempo de 5 días hábiles contados desde la
fecha en que se recibió y acusó de recibido la solicitud. Y el titular de
derechos contará con un periodo de 10 días hábiles contados a partir de que
reciba la solicitud de información faltante, para darle contestación a <b
style='mso-bidi-font-weight:normal'>HALER</b>.<span style='mso-spacerun:yes'> 
</span><o:p></o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36.0pt;
margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;vertical-align:
baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'>En
el caso de que <b style='mso-bidi-font-weight:normal'>HALER </b>tenga que dar
cumplimiento a la resolución que haya dado a la solicitud del titular de los
datos personales, lo hará dentro de los 15 días hábiles siguientes a la fecha
en que se comunique la respuesta al titular de los derechos.<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'>El
medio mediante el cual <b style='mso-bidi-font-weight:normal'>HALER</b> dará a
conocer la determinación adoptada a la solicitud, será el que el titular de
derechos haya mencionado en su solicitud para notificaciones, y los cuales
podrán ser mediante correo electrónico o de forma personal en el domicilio que
el titular de los datos personales mencione. En el caso de que mencione ambos
medios de forma indistinta, <b style='mso-bidi-font-weight:normal'>HALER</b>
podrá elegir la forma en que le notifique su determinación. En el caso de que
el titular de los datos personales haya solicitado el derecho de acceso, <b
style='mso-bidi-font-weight:normal'>HALER</b> le proporcionará la información
de forma electrónica o física, según lo establezca en su determinación y que
dependerá de los medios en los que se encuentre dicha información. <o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><i style='mso-bidi-font-style:normal'><span lang=ES
style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></i></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><i style='mso-bidi-font-style:normal'><span lang=ES
style='font-family:"Arial","sans-serif"'>MEDIOS PARA RECABAR DATOS PERSONALES.<o:p></o:p></span></i></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'>Los
datos personales que <b
style='mso-bidi-font-weight:normal'>HALER</b> recabe cuando usted los
proporcione podrán ser de forma directa, a través de terceros; personalmente, vía
telefónica, vía electrónica, mediante cualquier medio que así lo permita,
mediante medios impresos, cuando visite el sitio de internet de <b
style='mso-bidi-font-weight:normal'>HALER</b>, al momento de adquirir nuestros
productos o servicios,<b style='mso-bidi-font-weight:normal'> </b>al momento de
que nos proporcione sus productos o servicios, y a través de las demás fuentes que
están permitidas por la Ley.<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:15.0pt;margin-left:
0cm;text-align:justify;line-height:15.0pt;vertical-align:baseline'><i
style='mso-bidi-font-style:normal'><span lang=ES style='font-family:"Arial","sans-serif"'>PROTECCIÓN
DADA A LOS DATOS PERSONALES.<o:p></o:p></span></i></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif";
mso-bidi-font-weight:bold'>Los datos personales del titular son mantenidos en
estricta confidencialidad y se encuentran protegidos mediante el resguardo de
los datos en dispositivos físicos y electrónicos, a los que solamente se puede
tener acceso por medio de personas autorizadas y capacitadas para las funciones
que desempeñarán con los datos personales, quienes no le darán en ningún
momento uso para fines ilícitos o distintos a los mencionados en el presente
Aviso de Privacidad, ya que dicho personal se encuentra bajo supervisión y
vigilancia constante de <b>HALER</b>.<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif";
mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><i style='mso-bidi-font-style:normal'><span lang=ES
style='font-family:"Arial","sans-serif"'>USO DE COOKIES Y WEB BEACONS.<o:p></o:p></span></i></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><u><span lang=ES style='font-family:"Arial","sans-serif"'><o:p><span
 style='text-decoration:none'>&nbsp;</span></o:p></span></u></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'>La
página de internet de <b style='mso-bidi-font-weight:normal'>HALER</b> podrá
usar “cookies” con relación a ciertas funciones o características. Las
“cookies” son tipos específicos de información que una página de internet
transfiere al disco duro de la computadora del usuario con el propósito de
guardar ciertos registros. Las “cookies” hacen que el uso de una página de
internet sea más fácil con funciones tales como, guardar contraseñas y
preferencias mientras un visitante se encuentre en la página web. <o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'>Es
importante hacer notar que la página de internet puede llegar a permitir
publicidad o funciones de otras compañías que envíen “cookies” a su
computadora. Ni <b>HALER,</b> ni
la página de internet, controlan de manera alguna, la manera de uso de
“cookies” de otras compañías.<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'>Asimismo,
las páginas de internet pueden contener web beacons,
que son imágenes insertadas en la página o correo electrónico, que pueden ser
utilizadas para monitorear el comportamiento de un visitante, así como para
almacenar información sobre la dirección IP del usuario, duración del tiempo de
interacción de dicha página y el tipo de navegador utilizado, entre otros.<o:p></o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><i style='mso-bidi-font-style:normal'><span lang=ES
style='font-family:"Arial","sans-serif"'>MODIFICACIONES AL AVISO DE PRIVACIDAD.<o:p></o:p></span></i></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'>Cualquier
modificación a este Aviso de Privacidad podrá consultarse en la página web:<b>www.haler.com.mx<span
class=apple-converted-space>&nbsp;</span></b>o bien solicitándola por medios
electrónicos al correo previsto en este aviso:<b>direccion@haler.com.mx<o:p></o:p></b></span></p>

<p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;line-height:15.0pt;
vertical-align:baseline'><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:15.0pt;margin-left:
0cm;text-align:justify;line-height:15.0pt;vertical-align:baseline'><span
lang=ES style='font-family:"Arial","sans-serif"'>Nos reservamos el derecho de
efectuar en cualquier momento modificaciones o actualizaciones al&nbsp;presente
Aviso de Privacidad, así como para la atención de novedades legislativas,
normativas o jurisprudenciales, políticas&nbsp; internas, nuevos requerimientos
para proveer o contratar productos o servicios y prácticas del mercado en
relación con el presente Aviso de Privacidad.<o:p></o:p></span></p>

<p style='margin-top:0cm;margin-right:0cm;margin-bottom:15.0pt;margin-left:
0cm;text-align:justify;line-height:15.0pt;vertical-align:baseline'><u><span
lang=ES style='font-family:"Arial","sans-serif"'>Fecha de la última
actualización al presente Aviso de Privacidad:</span></u><span lang=ES
style='font-family:"Arial","sans-serif"'> 09 de diciembre de 2014.<o:p></o:p></span></p>

<p class=MsoNormal><span lang=ES style='font-family:"Arial","sans-serif"'><o:p>&nbsp;</o:p></span></p>

</div>
        </div>
    </div>
<style>
    #piebord>p{
        font-family: "Open Sans" !important;
    }
    a {
        color: #808080!important;
    }
    a:hover {
        color: #80aa22!important;
    }
</style>
<!-- /Blog Section with Sidebar -->
<?php get_footer(); ?>