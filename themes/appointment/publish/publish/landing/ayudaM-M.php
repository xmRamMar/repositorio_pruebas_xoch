<?php
/*
Template Name: Help Meet-Medic
*/ ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon">
    <title>Ayuda Meet-medic</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/pe.css">
    <link rel="stylesheet" href="assets/css/landing-page.min.css">
    
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">

</head>
<body class="landing-page">
    <?php include 'header.html';?>
    <div id="skrollr-body">
        <!-- Cover Slide -->
        <!-- Slogan Slide -->
        <div class="gap slide-slogan" id="slide-slogan">
            <div class="block-wrapper inner-content" style="padding-bottom: 0px!important;">
                <div class="slogan">
                    <p class="slogan-title">
                        Ayuda <span style="font-family:'Open Sans';"class="title-word">Meet-Medic</span>
                    </p>
                    <div class="slogan-separator"></div>
                    <p class="slogan-description">
                        Este es un instrumento con el cual los usuarios podrán guiarse para aprovechar las funcionalidades que ofrece Meet-Medic. Hemos separado las secciones de ayuda, por tipo de usuario, es decir, por: administración de la clínica, médico y pacientes; y adicionalmente, una sección en la que se abordan generalidades del sistema.
                    </p>
                    <div class="slogan-separator"></div>
                </div>
            </div>
        </div>
        <!-- Video Slide -->
        <a name="Generalidades" id="b"></a>
        <div class="gap slide-slogan">
            <div class="block-wrapper inner-content">
                <div class="slogan" style="padding: 0px;">
                    <p class="slogan-title">
                        <br>Registro y organización de las páginas</p>
                </div>
            </div>
        </div>
        <Iframe src="http://www.haler.com.mx/ayuda/" width="90%" height="500" style="margin-left: 5%; margin-right:5%;border: 0px;"></Iframe></br>
        <a name="Medicos" id="c"></a>
        <div class="gap slide-slogan">
            <div class="block-wrapper inner-content">
                <div class="slogan" style="padding: 0px;">
                    <p class="slogan-title">
                        <br>Médicos</p>
                </div>
            </div>
        </div>
        <Iframe src="http://www.haler.com.mx/ayudaMedico/" width="90%" height="800" style="margin-left: 5%; margin-right:5%;border: 0px;"></Iframe></br>
        <a name="UnidadesAdministrativas" id="d"></a>
        <div class="gap slide-slogan">
            <div class="block-wrapper inner-content">
                <div class="slogan" style="padding: 0px;">
                    <p class="slogan-title">
                        <br>Clínicas, Hospitales, Consultorios particulares (Unidades Administrativas)</p>
                </div>
            </div>
        </div>
        <Iframe src="http://www.haler.com.mx/ayudaClinica/" width="90%" height="800" style="margin-left: 5%; margin-right:5%;border: 0px;"></Iframe>
        <!-- Features Slide -->
        <div class="content slide-features" id="slide-features" style="padding-top: 0px!important;">
            
           


        <!-- Footer -->
        <?php include 'footer.html';?>
    </div>

    <!--prueba-->


        <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/slimscroll/jquery.slimscroll.min.js"></script>

    <!--Beyond Scripts-->
    <script src="assets/js/beyond.min.js"></script>

    <!--Page Related Scripts-->
    <script src="assets/js/bootbox/bootbox.js"></script>
    
    <!--fin prueba-->

    <!-- Main Refs -->
    <script src="assets/js/jquery-2.0.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Parallax -->
    <script type="text/javascript" src="assets/js/skrollr.min.js"></script>
    <!-- Scrooling -->
    <script src="assets/js/jquery.easing.1.3.js"></script>
    <!--Maps functions have been deleted-->
    <!-- App -->
    <script src="assets/js/app.js"></script>
    <script>
            $(function(){
                $("[data-load]").each(function(){
                    $(this).load($(this).data("load"), function(){
                    });
                });
            })
      </script>
</body>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-85593493-1', 'auto');
  ga('send', 'pageview');

</script>
</html>