﻿//Parallax Init
skrollr.init({
    smoothScrolling: false,
    mobileDeceleration: 0.004,
    forceHeight: false,
    mobileCheck: function () {
        return false;
    }
});

//Scroll Init
$('.page-scroll').on('click', function (e) {
    e.preventDefault();
    var $anchor = $(this);
    var id = $anchor[0].hash;
    $('html, body').stop().animate({ scrollTop: $(id).offset().top }, 1500, 'easeInOutExpo');
});

//Load and Play Video
function vidplay() {
    var video = document.getElementById("beyondadmin-video");
    var videobg = document.getElementById("video-bg");
    var video1 = document.getElementById("video-statement-playnow");
    videobg.style.display = 'none';
    $(".video-wrapper").css('display', 'block');
    video1.style.display = 'none';
}

function pauseVideo2() { 
    var vid = document.getElementById("beyondadmin-video"); 
    vid.pause(); 
}

