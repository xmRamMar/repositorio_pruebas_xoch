<?php
/*
Template Name: Página Carro
*/ ?>

<!--campo en el cual se adicionan-->

<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
    <style>
        .customize-support>img {
            width: 100% !important;
        }
    </style>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php 
	$appointment_options=theme_setup_data(); 
	$header_setting = wp_parse_args(  get_option( 'appointment_options', array() ), $appointment_options);
	if($header_setting['upload_image_favicon']!=''){ ?>
	<link rel="shortcut icon" href="<?php  echo $header_setting['upload_image_favicon']; ?>" /> 
	<?php } ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?> >

<?php if ( get_header_image() != '') {?>
<div class="header-img">
	<div class="header-content">
		<?php if($header_setting['header_one_name'] != '') { ?>
		<h1><?php echo $header_setting['header_one_name'];?></h1>
		<?php }  if($header_setting['header_one_text'] != '') { ?>
		<h3><?php echo $header_setting['header_one_text'];?></h3>
		<?php } ?>
	</div>
	<img class="img-responsive" src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" />
</div>
<?php } ?>

<div class="Service-section">
<!--Logo & Menu Section-->	
<nav class="navbar navbar-default">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
				<?php if($header_setting['text_title'] == 1) { ?>
				<h1><a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<?php
					 if($header_setting['enable_header_logo_text'] == 1) 
					{ echo "<div class=appointment_title_head>" . get_bloginfo( ). "</div>"; }
					elseif($header_setting['upload_image_logo']!='') 
					{ ?>
					<img class="img-responsive" src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/Logo-Haler.png" style="height:<?php echo $header_setting['height']; ?>px; width:<?php echo $header_setting['width']; ?>px;"/>
					<?php } else { ?>
					<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/logo.png">
					<?php } ?>
				</a></h1>
				<?php } ?>	
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only"><?php _e('Toggle navigation','appointment'); ?></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		
		<?php 
				$facebook = $header_setting['social_media_facebook_link'];
				$twitter = $header_setting['social_media_twitter_link'];
				$linkdin = $header_setting['social_media_linkedin_link'];
				
				$social = '<ul id="%1$s" class="%2$s">%3$s';
				if($header_setting['header_social_media_enabled'] == 0 )
				{
					$social .= '<ul class="head-contact-social">';

					if($header_setting['social_media_facebook_link'] != '') {
					$social .= '<li class="facebook"><a href="https://www.facebook.com/TI-929685520419250/"';
						if($header_setting['facebook_media_enabled']==1)
						{
						 $social .= 'target="_blank"';
						}
					$social .='><i class="fa fa-facebook"></i></a></li>';
					}
					if($header_setting['social_media_twitter_link']!='') {
					$social .= '<li class="twitter"><a href="'.$twitter.'"';
						if($header_setting['twitter_media_enabled']==1)
						{
						 $social .= 'target="_blank"';
						}
					$social .='><i class="fa fa-twitter"></i></a></li>';
					
					}
					if($header_setting['social_media_linkedin_link']!='') {
					$social .= '<li class="linkedin"><a href="'.$linkdin.'"';
						if($header_setting['linkedin_media_enabled']==1)
						{
						 $social .= 'target="_blank"';
						}
					$social .='><i class="fa fa-linkedin"></i></a></li>';
					}
					$social .='</ul>'; 
					
			}
			$social .='</ul>'; 
		
		?>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<?php wp_nav_menu( array(  
				'theme_location' => 'primary',
				'container'  => '',
				'menu_class' => 'nav navbar-nav navbar-right',
				'fallback_cb' => 'webriti_fallback_page_menu',
				'items_wrap'  => $social,
				'walker' => new webriti_nav_walker()
				 ) );
				?>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>	
<!--/Logo & Menu Section-->	
<div class="clearfix"></div>

<!--fin-->


<style>
    /*!*/
    .rowimg {
        display: inline-block;
        width: 100%;
        margin-left:15% ;
        margin-right:15%;
    }
    .imghover {
        display: inline-block;
        width: 23%;
    }
    .imghover:hover div{
        width: 18.35%;
        z-index: 100;
    display: initial;
    position:absolute;  
    }
    .innerdiv {
        text-align: center;
        color: #fff;
        font-family: Verdana;
        font-size: x-large;
        vertical-align: middle!important;
        width: 16.67%;
        background-color: rgba(138, 106, 46, 0.39);
    }
    
        /*estilo para el contact form*/
    input[type="submit"] {
       box-shadow: 0 3px 0 0 #fff; /*cambiar_sombra_input!siempre blanco*/
    }
    element.style,.hc_scrollup{
        background-color: #000000; /*cambiar_arriba*/
    }
    .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, #piebord>p ,.smform-submitbtn-cont>input {
        background: #fff !important; /*cambiar_input_color!siempre blanco*/
        color: #000000!important;/*cambiar_color_texto*/
    }
    .smform-field-label.required:after {
        color: #fff; /*cambiar_asteriscos*/
    }
    /*FIN estilo para el contact form FIN*/
    .row > div>img {
        display: block;
        width: 20%;
        margin-left: 0px;
        margin-right: 0px;
        padding-left: 0px;
        padding-right: 0px;
        
    }
	
	.page-builder{
		margin: 5% 0 0;
	}	
	
    .page-builder-colour {
    
    }
    li {
        list-style-type:none;
        font-size: smaller;
    }
    h4 {
        color: #000000; /*cambiar*/
    }
    h3 {
        font-weight: 700;
        border-bottom: 5px solid #000000;/*cambiar*/
    }
    .col-md-6 {
        text-align: center;
        flex-align: center !important;
    }
	
	.Service-section {
		margin:0;
	}	
	
    .service-iconx {
        margin-bottom: 3%;
    }
    .service-iconx>img {
        display: flexbox;
        border-radius: 50%;
        padding: 2%;
        width: 20%;
        height: 20%;
        border: 3px solid #d0d0d0;
    }
    .row:nth-last-child(n) :hover .service-iconx img{ /*.service-area:hover .service-icon i !!!!VERIFICAR FUNCIONAMIENTO*/
         list-style: none;
        display: flexbox;
        border-radius: 50%;
        border: 4px solid #000000;/*cambiar*/
    }
    .media {
        display: block;
    }
    table,tr,td {
        border: 0;
        background: #ffffff;
        width:100%; 
        border-color:  #ffffff!important;
        vertical-align: top;
        text-align: center;
    }
    
    .row .col-md-4:nth-child(1) {
        display: block !important;
        text-align: center;
}
    .row .col-md-4:nth-child(2) {
        display: block !important;
        text-align: center;
}
    .row .col-md-4:nth-child(3) {
        display: block !important;
        text-align: center;
}
    

/* Estilos galeria TI*/

.galerias_cont {
	width: 100%;
	/*margin-left:19%;
	margin-right:19%;*/
    margin-left:5%;
    margin-right: 5%;
}

.galeria_ti{
    width: 22.7%;
    padding: 1%;
	position: relative;
	float: left;
}
 
.galeria_ti>div>div>form>div>div>span {
	display:none!important;
}    
    
</style>
<!--div class="page-builder"> 
	<div class="container">
        <img src="../wp-content/themes/appointment/images/im/TI/TI.png" width="100%" height="auto" alt="i" >
    </div>
</div-->
<a name="Nosotros" id="a"></a>
<!-- Blog Section -->
<div class="page-builder">
	<div class="container">
		<div class="row">
            			

			<!--Sidebar Area-->
		</div>
        <!--div class="row">
            <div class="col-md-6">
                <div class="service-iconx">
                            <img src="../wp-content/themes/appointment/images/im/icons/idea.png" width="100%"alt="i" >
                        </div>
                        <div class="media-body">
                            <h3>Misión</h3>
                        </div>
                        <div class="media-body">
                            <p>Diseñar, desarrollar soluciones y servicios software de calidad que optimicen los procesos de nuestros clients para mejorar su interacción con el usuario final.</p> 
                        </div>
            </div>
            
            <div class="col-md-6">
                <div class="service-iconx">
                    <img src="../wp-content/themes/appointment/images/im/icons/cohete.png" width="100%"alt="i" >
                </div>
                        <div class="media-body">
                            <h3>Visión</h3>
                        </div>
                        <div class="media-body">
                            <p>Ser la primera opción para las empresas que buscan optimizar sus procesos administrativos y operativos mediante software de calidad que generen experiencias únicas al usuario final.</p>
                        </div>
            </div>
        </div-->

        <!--div class="row">
            <div class="col-md-6" style="margin: 0;
    left: 25%;">
                <div class="service-iconx">
                            <img src="../wp-content/themes/appointment/images/im/icons/trabajo.png" width="100%"alt="i" >
                        </div>
                        <div class="media-body">
                            <h3>Trabajos Realizados</h3>
                        </div>
                        <div class="media-body">
                            <table>
                            <tr>
                                <td>
                                    <h4>Venta</h4>
                                        <p>App IOS/Android</p>
                                        <p>Aplicaciones web</p>
                                        <p>ERP</p>
                                </td>
                                <td>
                                    <h4>Renta</h4>
                                        <p>Catalogic</p>
                                        <p>Meet-medic</p>
                                        <p>School connection</p>
                                </td>
                            </tr>
                            </table>
                            
                            
                            
                             
                        </div>
            </div>
        </div-->
		<?php echo do_shortcode("[woocommerce_cart]"); ?>
	</div>
</div>
        </br>
        </br>
        </br>
<!--div class="page-builder">
	<div class="container">
        <img src="../wp-content/themes/appointment/images/im/TI/Certificaciones.png" width="100%" height="auto" alt="i" >
    </div>
</div>
<div class="page-builder">
	<div class="container">
            <div class="galerias_cont">
                <div class="galeria_ti"><!--?php echo photo_gallery(1); ?></div>
                <div class="galeria_ti"><!--?php echo photo_gallery(5); ?></div>
                <div class="galeria_ti"><!--?php echo photo_gallery(3); ?></div>
                <div class="galeria_ti"><!--?php echo photo_gallery(4); ?></div>
            </div>
    </div>
</div-->
<a name="Contacto" id="b"></a>
        
<style>
    #piebord > p{
        background-color: #fff!important;
        border-top: 5px #000 solid;
    }
</style>
<!-- /Blog Section with Sidebar -->
<?php get_footer(); ?>