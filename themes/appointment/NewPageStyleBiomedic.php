<?php
/*
Template Name: Página Biomedic
*/ ?>

<!--campo en el cual se adicionan-->

<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
    <style>
        .customize-support>img {
            width: 100% !important;
        }
    </style>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php 
	$appointment_options=theme_setup_data(); 
	$header_setting = wp_parse_args(  get_option( 'appointment_options', array() ), $appointment_options);
	if($header_setting['upload_image_favicon']!=''){ ?>
	<link rel="shortcut icon" href="<?php  echo $header_setting['upload_image_favicon']; ?>" /> 
	<?php } ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?> >

<?php if ( get_header_image() != '') {?>
<div class="header-img">
	<div class="header-content">
		<?php if($header_setting['header_one_name'] != '') { ?>
		<h1><?php echo $header_setting['header_one_name'];?></h1>
		<?php }  if($header_setting['header_one_text'] != '') { ?>
		<h3><?php echo $header_setting['header_one_text'];?></h3>
		<?php } ?>
	</div>
	<img class="img-responsive" src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" />
</div>
<?php } ?>


<!--Logo & Menu Section-->	
<nav class="navbar navbar-default">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
				<?php if($header_setting['text_title'] == 1) { ?>
				<h1><a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<?php
					 if($header_setting['enable_header_logo_text'] == 1) 
					{ echo "<div class=appointment_title_head>" . get_bloginfo( ). "</div>"; }
					elseif($header_setting['upload_image_logo']!='') 
					{ ?>
					<img class="img-responsive" src="../wp-content/themes/appointment/images/im/icons/Logo Biomedic.png" style="height:<?php echo $header_setting['height']; ?>px; width:<?php echo $header_setting['width']; ?>px;"/>
					<?php } else { ?>
					<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/logo.png">
					<?php } ?>
				</a></h1>
				<?php } ?>	
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only"><?php _e('Toggle navigation','appointment'); ?></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		
		<?php 
				$facebook = $header_setting['social_media_facebook_link'];
				$twitter = $header_setting['social_media_twitter_link'];
				$linkdin = $header_setting['social_media_linkedin_link'];
				
				$social = '<ul id="%1$s" class="%2$s">%3$s';
				if($header_setting['header_social_media_enabled'] == 0 )
				{
					$social .= '<ul class="head-contact-social">';

					if($header_setting['social_media_facebook_link'] != '') {
					$social .= '<li class="facebook"><a href="https://www.facebook.com/Biomedic-726152594186704/?skip_nax_wizard=true"';
						if($header_setting['facebook_media_enabled']==1)
						{
						 $social .= 'target="_blank"';
						}
					$social .='><i class="fa fa-facebook"></i></a></li>';
					}
					if($header_setting['social_media_twitter_link']!='') {
					$social .= '<li class="twitter"><a href="'.$twitter.'"';
						if($header_setting['twitter_media_enabled']==1)
						{
						 $social .= 'target="_blank"';
						}
					$social .='><i class="fa fa-twitter"></i></a></li>';
					
					}
					if($header_setting['social_media_linkedin_link']!='') {
					$social .= '<li class="linkedin"><a href="'.$linkdin.'"';
						if($header_setting['linkedin_media_enabled']==1)
						{
						 $social .= 'target="_blank"';
						}
					$social .='><i class="fa fa-linkedin"></i></a></li>';
					}
					$social .='</ul>'; 
					
			}
			$social .='</ul>'; 
		
		?>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<?php wp_nav_menu( array(  
				'theme_location' => 'primary',
				'container'  => '',
				'menu_class' => 'nav navbar-nav navbar-right',
				'fallback_cb' => 'webriti_fallback_page_menu',
				'items_wrap'  => $social,
				'walker' => new webriti_nav_walker()
				 ) );
				?>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>	
<!--/Logo & Menu Section-->	
<div class="clearfix"></div>

<!--fin-->


<style>
    /*!*/
    @media (max-width: 980px) {
        .rowimg {
        width: 60%!important;
        margin-left:20% !important;
        margin-right:20%!important;
        display: block!important;
    }
        .imghover {
        padding: 10%;
            width: 100%!important;
            height: 18em!important;
        }
        
    }
    .rowimg div {
        margin: 0px!important;
        padding: 0px!important;
        border: 0px !important;
        
        
    }
    .rowimg {
        width: 93%;
        margin-left:3.5% ;
        margin-right:3.5%;
        display: inline-flex;
    }
    .imghover {
        background-size: cover;
        background-repeat: no-repeat;
        width: 33%;
        height: 25em;
        padding: 0px;
        display: table;
    }
    .imghover > div {
        font-size: 0px;
        text-align: center;
        display: table-cell;
        vertical-align: middle;
        position:initial;
    }
    .innerdiv {
        
        width: 100%;
        height: 100%;
        
    }

    .innerdiv span {
        position:initial;
        vertical-align: middle;
        display: inline-block;

    }
    
    .innerdiv:hover {
        position:initial;
        background-color: rgba(255, 192, 32, 0.60);
        color: #fff;
        font-family: Verdana;
        font-size: x-large;
        
    }
    /*estilo para el contact form*/
    input[type="submit"] {
       box-shadow: 0 3px 0 0 #fff; /*cambiar_sombra_input!siempre blanco*/
    }
    element.style,.hc_scrollup{
        background-color: #f2cf3c; /*cambiar_arriba*/
    }
    .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, #piebord>p ,.smform-submitbtn-cont>input {
        background: #fff !important; /*cambiar_input_color!siempre blanco*/
        color: #f2cf3c!important;/*cambiar_color_texto*/
    }
    .smform-field-label.required:after {
        color: #fff; /*cambiar_asteriscos*/
    }
    /*FIN estilo para el contact form FIN*/
    .row > div>img {
        display: block;
        width: 20%;
        margin-left: 0px;
        margin-right: 0px;
        padding-left: 0px;
        padding-right: 0px;
        
    }
    .page-builder-colour {
    
    }
    li {
        list-style-type:none;
        font-size: smaller;
    }
    h4 {
        color: #f2cf3c; /*cambiar*/
    }
    h3 {
        font-weight: 700;
        border-bottom: 5px solid #f2cf3c;/*cambiar*/
    }
    .col-md-6 {
        text-align: center;
        flex-align: center !important;
    }
    .service-iconx {
        margin-bottom: 3%;
    }
    .service-iconx>img {
        display: flexbox;
        border-radius: 50%;
        padding: 2%;
        width: 20%;
        height: 20%;
        border: 3px solid #d0d0d0;
    }
    .row:nth-last-child(n) :hover .service-iconx img{ /*.service-area:hover .service-icon i !!!!VERIFICAR FUNCIONAMIENTO*/
         list-style: none;
        display: flexbox;
        border-radius: 50%;
        border: 4px solid #f2cf3c;/*cambiar*/
    }
    .media {
        display: block;
    }
</style>
<div class="page-builder"> 
	<div class="container">
        <img src="../wp-content/themes/appointment/images/im/Biomedic/Biomedic.png" width="100%" height="auto" alt="i" >
    </div>
</div>
<a name="Nosotros" id="a"></a>
<!-- Blog Section -->
<div class="page-builder">
	<div class="container">

<div class="row">
            <div class="col-md-6">
                <div class="service-iconx">
                            <img src="../wp-content/themes/appointment/images/im/icons/idea.png" width="100%"alt="i" >
                        </div>
                        <div class="media-body">
                            <h3>Misión</h3>
                        </div>
                        <div class="media-body">
                            <p>Diseñar, desarrollar y dar asistencia técnica a sistemas tecnológicos como prótesis, órtesis, equipos y sistemas para dar servicios de calidad a las personas con alguna necesidad médica o específica.</p> 
                        </div>
            </div>
            
            <div class="col-md-6">
                <div class="service-iconx">
                    <img src="../wp-content/themes/appointment/images/im/icons/cohete.png" width="100%"alt="i" >
                </div>
                        <div class="media-body">
                            <h3>Visión</h3>
                        </div>
                        <div class="media-body">
                            <p>Ser una empresa que diseñe, desarrolle y repare sistemas tecnológicos que ayuden a las personas en el ámbito de la medicina.</p>
                        </div>
            </div>
        </div>

	</div>
</div>
<div class="page-builder">
	<div class="container">
		<div class="rowimg">
            <div class="imghover" style="background-image: url('../wp-content/themes/appointment/images/im/Biomedic/uno.png')">
                <div class="innerdiv">
                    <span>
                        Mantenimiento de equipo médico
                    </span>
                </div>
            </div>
            <div class="imghover" style="background-image: url('../wp-content/themes/appointment/images/im/Biomedic/dos.png')">
                <div class="innerdiv" >
                    <span>Investigación de:<span style="text-align: left;margin-left: 20%;">Protesis</br>Silla de ruedas</br>Silla Mioeléctrica</span>
                    </span>   
                </div>
            </div>
            <div class="imghover" style="background-image: url('../wp-content/themes/appointment/images/im/Biomedic/tres.png') ">
                <div class="innerdiv">
                    <span>Comercialización</br></br>Búsqueda de socios comerciales</span>                    
                </div>
            </div>
        </div>
    </div>
</div>
<a name="Contacto" id="b"></a>
<!--edición fondos-->
<?php 
$appointment_options=theme_setup_data();
$callout_setting = wp_parse_args(  get_option( 'appointment_options', array() ), $appointment_options );
if($callout_setting['home_call_out_area_enabled'] == 0 ) { 
 $imgURL = $callout_setting['callout_background'];
 if($imgURL != '') { ?>
<div class="callout-section" style="background-image:url('../wp-content/themes/appointment/images/im/Amarillo.png'); background-repeat: no-repeat; background-position: top left; background-attachment: fixed;">

<?php } 
else
{ ?> 
<div class="callout-section" style="background-color:#ccc;">
<?php } ?>
	<div class="overlay">
		<div class="container">
			<div class="row">	
				<div class="col-md-12">	
						
						<h1><?php echo $callout_setting['home_call_out_title'];?></h1>
						 <p><?php echo $callout_setting['home_call_out_description']; ?></p>
					
						<div><?php if ( function_exists( 'smuzform_render_form' ) ) { smuzform_render_form('206'); }?></div>
				</div>	
			</div>			
		
		</div>
			
	</div>	
</div> 
<!-- /Callout Section -->
<div class="clearfix"></div>
<?php } ?>
<!--edición fondos-->
<style>
    #piebord > p{
        border-top: 5px #f2cf3c solid;
    }
</style>
<!-- /Blog Section with Sidebar -->
<?php get_footer(); ?>