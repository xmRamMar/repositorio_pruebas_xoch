<?php
/*
Template Name: Participantes
*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
	<script>
		
		
		window.onload = checkCookie;
		
		function checarCookie(){
			
			var cokieee= document.cookie;
			alert("cookies-:"+cokieee);
		}
		function checkCookie() {
			var username = getCookie("login");
			if (username != "") {
				
			}else{
				location.href="http://www.haler.com.mx/login-concurso-elretaildelfuturo/";
			}
		}; 
		
		function getCookie(cname) {
			var name = cname + "=";
			var decodedCookie = decodeURIComponent(document.cookie);
			var ca = decodedCookie.split(';');
			for(var i = 0; i <ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ') {
					c = c.substring(1);
				}
				if (c.indexOf(name) == 0) {
					return c.substring(name.length, c.length);
				}
			}
			return "";
		}; 
		
				
	</script>
    <style>
        .customize-support>img {
            width: 100% !important;
        }
    </style>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php 
    $appointment_options=theme_setup_data(); 
    $header_setting = wp_parse_args(  get_option( 'appointment_options', array() ), $appointment_options);
    if($header_setting['upload_image_favicon']!=''){ ?>
    <link rel="shortcut icon" href="<?php  echo $header_setting['upload_image_favicon']; ?>" /> 
    <?php } ?>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <style>
        #fine-uploader-s3 .preview-link {
            display: block;
            height: 100%;
            width: 100%;
        }
		
		.modal_participante{
			width:60%!important;
		}
		
		
		.titulo_login{
			font-size: 45px!important;
		}
		
		.contenedor_participantes {
			width: 90%;
			margin: 0 5%;
		}
		
		.contenedor_participantes table{
			table-layout:fixed;
		}		
		
		.overflow {
			height:40px;
			text-align:left;
			overflow:hidden;
		}
		
		.celdaes{
			overflow: hidden;
			white-space:nowrap;
			text-overflow: ellipsis;
		}
		
		.oculta{
			display: none;
		}
		
		.infolbl{
			position:relative;
			float: left;
			margin-bottom: 10px;
			width:30%;
			opacity: 0.6;
		}
		
		.infoInput{
			position:relative;
			float: left;
			width:70%;
			height:auto;
			padding: 0px;
			margin-bottom: 10px;
			display: inline-block;
		}
		.infofooter{
			margin-top: 5%;
			border: none!important;
		}
		
		.infoTxtArea{
			resize:none;
			margin-bottom:5%;
		}
		
		.ver{
			width:50%;
			margin:0 25%;
		}
		
		.encabezado_modal{
			background-color: #46b8da;
			color: white;
			text-align:center;
			border-radius: 6px;
		} 
		
		.encabezado_modal h4{
			font-size: 20px;
			font-weight: bold;
		}
		
		.btn_aceptar_modal{
			background-color: #80aa22!important;
			color: white!important;
			font-weight: bold!important;
		}
		
		
    </style>
    <?php wp_head(); ?>
    <script src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/js/jquery.js" ></script>
    <script src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/js/spin.min.js" ></script>
    <!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
    <link href="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/css/fine-uploader.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/css/bootstrap.css"  type="text/css">

    <!-- Fine Uploader S3 JS file
    ====================================================================== -->
    <script src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/js/s3.fine-uploader.min.js"></script>
    
     <!-- Fine Uploader Customized Gallery template
    ====================================================================== -->
    <!--
    This is a legacy template and is not meant to be used in new Fine Uploader integrated projects.
    Read the "Getting Started Guide" at http://docs.fineuploader.com/quickstart/01-getting-started.html
    if you are not yet familiar with Fine Uploader UI.
-->


</head>
    <body <?php body_class(); ?> >

<?php if ( get_header_image() != '') {?>
<div class="header-img">
    <div class="header-content">
        <?php if($header_setting['header_one_name'] != '') { ?>
        <h1><?php echo $header_setting['header_one_name'];?></h1>
        <?php }  if($header_setting['header_one_text'] != '') { ?>
        <h3><?php echo $header_setting['header_one_text'];?></h3>
        <?php } ?>
    </div>
    <img class="img-responsive" src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" />
</div>
<?php } ?>

<!--Logo & Menu Section-->  
<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
                <?php if($header_setting['text_title'] == 1) { ?>
                <h1><a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <?php
                     if($header_setting['enable_header_logo_text'] == 1) 
                    { echo "<div class=appointment_title_head>" . get_bloginfo( ). "</div>"; }
                    elseif($header_setting['upload_image_logo']!='') 
                    { ?>
                    <img class="img-responsive" src="../wp-content/themes/appointment/images/im/icons/Logo TI.png" style="height:<?php echo $header_setting['height']; ?>px; width:<?php echo $header_setting['width']; ?>px;"/>
                    <?php } else { ?>
                    <img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/logo.png">
                    <?php } ?>
                </a></h1>
                <?php } ?>  
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only"><?php _e('Toggle navigation','appointment'); ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        
        <?php 
                $facebook = $header_setting['social_media_facebook_link'];
                $twitter = $header_setting['social_media_twitter_link'];
                $linkdin = $header_setting['social_media_linkedin_link'];
                
                $social = '<ul id="%1$s" class="%2$s">%3$s';
                if($header_setting['header_social_media_enabled'] == 0 )
                {
                    $social .= '<ul class="head-contact-social">';

                    if($header_setting['social_media_facebook_link'] != '') {
                    $social .= '<li class="facebook"><a href="https://www.facebook.com/TI-929685520419250/"';
                        if($header_setting['facebook_media_enabled']==1)
                        {
                         $social .= 'target="_blank"';
                        }
                    $social .='><i class="fa fa-facebook"></i></a></li>';
                    }
                    if($header_setting['social_media_twitter_link']!='') {
                    $social .= '<li class="twitter"><a href="'.$twitter.'"';
                        if($header_setting['twitter_media_enabled']==1)
                        {
                         $social .= 'target="_blank"';
                        }
                    $social .='><i class="fa fa-twitter"></i></a></li>';
                    
                    }
                    if($header_setting['social_media_linkedin_link']!='') {
                    $social .= '<li class="linkedin"><a href="'.$linkdin.'"';
                        if($header_setting['linkedin_media_enabled']==1)
                        {
                         $social .= 'target="_blank"';
                        }
                    $social .='><i class="fa fa-linkedin"></i></a></li>';
                    }
                    $social .='</ul>'; 
                    
            }
            $social .='</ul>'; 
        
        ?>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <?php wp_nav_menu( array(  
                'theme_location' => 'primary',
                'container'  => '',
                'menu_class' => 'nav navbar-nav navbar-right',
                'fallback_cb' => 'webriti_fallback_page_menu',
                'items_wrap'  => $social,
                'walker' => new webriti_nav_walker()
                 ) );
                ?>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>  
<!--/Logo & Menu Section--> 
<div class="clearfix"></div>

<!--fin-->


<style>
   .wating{
    background: #FFFFFF;
    opacity: 0.5;
}

.input_text{
	border:none;
}

</style>
<a name="Nosotros" id="a"></a>

     

        
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
<div class="container" >
    <div class="row" style="padding-top: 15px;">
        <div class="col-md-2">
            <a href="http://www.haler.com.mx/ti/">
            <img src="../wp-content/themes/appointment/images/TI.png" width="100px" alt="i">
            </a>
        </div>

        <div class="col-md-2">
            <a href="http://www.haler.com.mx/the-engineers/">
            <img src="../wp-content/themes/appointment/images/TE.png" width="100px" alt="i">
            </a>
        </div>      

        <div class="col-md-2">
        <a href="http://www.haler.com.mx/doin/">
            <img src="../wp-content/themes/appointment/images/DO.png" width="100px" alt="i">
            </a>
        </div>

        <div class="col-md-2">
            <a href="http://www.haler.com.mx/biomedic/">
            <img src="../wp-content/themes/appointment/images/BI.png" width="100px" alt="i">
            </a>
        </div>

        <div class="col-md-2">
            <a href="http://www.haler.com.mx/industry/">
            <img src="../wp-content/themes/appointment/images/IN.png" width="100px" alt="i">
            </a>
        </div>

        <div class="col-md-2">
            <a href="http://www.haler.com.mx/academy/">
            <img src="../wp-content/themes/appointment/images/AC.png" width="100px" alt="i">
            </a>
        </div>
    </div>
</div>
<!-- Blog Section -->
<div class="page-builder jumbotron">
    <div class="contenedor_participantes">
        <h1  class="text-center titulo_login" style="padding-top: 20px;">El Retail Del Futuro</h1>
        <h3  class="text-center" >Participantes</h3>
	<div>
		<?php 
			include 'connection.php';
			?>
	</div>

              <!-- Fine Uploader DOM Element
    ====================================================================== -->
   
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
    <!-- Your code to create an instance of Fine Uploader and bind to the DOM/template
    ====================================================================== -->

    
    </div>


</div>

<a name="Contacto" id="b"></a>
        <!--edición fondos-->
<?php 
$appointment_options=theme_setup_data();
$callout_setting = wp_parse_args(  get_option( 'appointment_options', array() ), $appointment_options );
if($callout_setting['home_call_out_area_enabled'] == 0 ) { 
 $imgURL = $callout_setting['callout_background'];
 if($imgURL != '') { ?>
<div class="callout-section" style="background-image:url('../wp-content/themes/appointment/images/im/Negro.png'); background-repeat: no-repeat; background-position: top left; background-attachment: fixed;">

<?php } 
else
{ ?> 
<div class="callout-section" style="background-color:#ccc;">
<?php } ?>
    <div class="overlay">
        <div class="container">
            <div class="row">   
                <div class="col-md-12"> 
                        
                        <h1><?php echo $callout_setting['home_call_out_title'];?></h1>
                         <p><?php echo $callout_setting['home_call_out_description']; ?></p>
                    
                        <div><?php if ( function_exists( 'smuzform_render_form' ) ) { smuzform_render_form('204'); }?></div>
                </div>  
            </div>          
        
        </div>
            
    </div>  
</div> 
<!-- /Callout Section -->
<div class="clearfix"></div>
<?php } ?>
<!--edición fondos-->
<style>
    #piebord > p{
        background-color: #fff!important;
        border-top: 5px #000 solid;
    }
</style>
<!-- /Blog Section with Sidebar -->

<?php get_footer(); ?>