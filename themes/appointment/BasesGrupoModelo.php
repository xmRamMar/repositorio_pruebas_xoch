<?php
/*
Template Name: Página Convocatoria
*/ ?>

<!--campo en el cual se adicionan-->

<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
    <style>
        .customize-support>img {
            width: 100% !important;
        }
    </style>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php 
	$appointment_options=theme_setup_data(); 
	$header_setting = wp_parse_args(  get_option( 'appointment_options', array() ), $appointment_options);
	if($header_setting['upload_image_favicon']!=''){ ?>
	<link rel="shortcut icon" href="<?php  echo $header_setting['upload_image_favicon']; ?>" /> 
	<?php } ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?> >

<?php if ( get_header_image() != '') {?>
<div class="header-img">
	<div class="header-content">
		<?php if($header_setting['header_one_name'] != '') { ?>
		<h1><?php echo $header_setting['header_one_name'];?></h1>
		<?php }  if($header_setting['header_one_text'] != '') { ?>
		<h3><?php echo $header_setting['header_one_text'];?></h3>
		<?php } ?>
	</div>
	<img class="img-responsive" src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" />
</div>
<?php } ?>

<!--Logo & Menu Section-->	
<nav class="navbar navbar-default">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
				<?php if($header_setting['text_title'] == 1) { ?>
				<h1><a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<?php
					 if($header_setting['enable_header_logo_text'] == 1) 
					{ echo "<div class=appointment_title_head>" . get_bloginfo( ). "</div>"; }
					elseif($header_setting['upload_image_logo']!='') 
					{ ?>
					<img class="img-responsive" src="../wp-content/themes/appointment/images/im/icons/Logo TI.png" style="height:<?php echo $header_setting['height']; ?>px; width:<?php echo $header_setting['width']; ?>px;"/>
					<?php } else { ?>
					<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/logo.png">
					<?php } ?>
				</a></h1>
				<?php } ?>	
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only"><?php _e('Toggle navigation','appointment'); ?></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		
		<?php 
				$facebook = $header_setting['social_media_facebook_link'];
				$twitter = $header_setting['social_media_twitter_link'];
				$linkdin = $header_setting['social_media_linkedin_link'];
				
				$social = '<ul id="%1$s" class="%2$s">%3$s';
				if($header_setting['header_social_media_enabled'] == 0 )
				{
					$social .= '<ul class="head-contact-social">';

					if($header_setting['social_media_facebook_link'] != '') {
					$social .= '<li class="facebook"><a href="https://www.facebook.com/TI-929685520419250/"';
						if($header_setting['facebook_media_enabled']==1)
						{
						 $social .= 'target="_blank"';
						}
					$social .='><i class="fa fa-facebook"></i></a></li>';
					}
					if($header_setting['social_media_twitter_link']!='') {
					$social .= '<li class="twitter"><a href="'.$twitter.'"';
						if($header_setting['twitter_media_enabled']==1)
						{
						 $social .= 'target="_blank"';
						}
					$social .='><i class="fa fa-twitter"></i></a></li>';
					
					}
					if($header_setting['social_media_linkedin_link']!='') {
					$social .= '<li class="linkedin"><a href="'.$linkdin.'"';
						if($header_setting['linkedin_media_enabled']==1)
						{
						 $social .= 'target="_blank"';
						}
					$social .='><i class="fa fa-linkedin"></i></a></li>';
					}
					$social .='</ul>'; 
					
			}
			$social .='</ul>'; 
		
		?>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<?php wp_nav_menu( array(  
				'theme_location' => 'primary',
				'container'  => '',
				'menu_class' => 'nav navbar-nav navbar-right',
				'fallback_cb' => 'webriti_fallback_page_menu',
				'items_wrap'  => $social,
				'walker' => new webriti_nav_walker()
				 ) );
				?>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>	
<!--/Logo & Menu Section-->	
<div class="clearfix"></div>

<!--fin-->


<style>
    /*!*/
    .rowimg {
        display: inline-block;
        width: 100%;
        margin-left:15% ;
        margin-right:15%;
    }
    .imghover {
        display: inline-block;
        width: 23%;
    }
    .imghover:hover div{
        width: 18.35%;
        z-index: 100;
    display: initial;
    position:absolute;  
    }
    .innerdiv {
        text-align: center;
        color: #fff;
        font-family: Verdana;
        font-size: x-large;
        vertical-align: middle!important;
        width: 16.67%;
        background-color: rgba(138, 106, 46, 0.39);
    }
    
        /*estilo para el contact form*/
    input[type="submit"] {
       box-shadow: 0 3px 0 0 #fff; /*cambiar_sombra_input!siempre blanco*/
    }
    element.style,.hc_scrollup{
        background-color: #000000; /*cambiar_arriba*/
    }
    .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, #piebord>p ,.smform-submitbtn-cont>input {
        background: #fff !important; /*cambiar_input_color!siempre blanco*/
        color: #000000!important;/*cambiar_color_texto*/
    }
    .smform-field-label.required:after {
        color: #fff; /*cambiar_asteriscos*/
    }
    /*FIN estilo para el contact form FIN*/
    .row > div>img {
        display: block;
        width: 20%;
        margin-left: 0px;
        margin-right: 0px;
        padding-left: 0px;
        padding-right: 0px;
        
    }
    .page-builder-colour {
    
    }
    li {
        list-style-type:none;
        font-size: smaller;
    }
    h4 {
        color: #000000; /*cambiar*/
    }
    h3 {
        font-weight: 700;
        border-bottom: 5px solid #000000;/*cambiar*/
    }
    .col-md-6 {
        text-align: center;
        flex-align: center !important;
    }
    .service-iconx {
        margin-bottom: 3%;
    }
    .service-iconx>img {
        display: flexbox;
        border-radius: 50%;
        padding: 2%;
        width: 20%;
        height: 20%;
        border: 3px solid #d0d0d0;
    }
    .row:nth-last-child(n) :hover .service-iconx img{ /*.service-area:hover .service-icon i !!!!VERIFICAR FUNCIONAMIENTO*/
         list-style: none;
        display: flexbox;
        border-radius: 50%;
        border: 4px solid #000000;/*cambiar*/
    }
    .media {
        display: block;
    }
    table,tr,td {
        border: 0;
        background: #ffffff;
        width:100%; 
        border-color:  #ffffff!important;
        vertical-align: top;
        text-align: center;
    }
    
    .row .col-md-4:nth-child(1) {
        display: block !important;
        text-align: center;
}
    .row .col-md-4:nth-child(2) {
        display: block !important;
        text-align: center;
}
    .row .col-md-4:nth-child(3) {
        display: block !important;
        text-align: center;
}
    

/* Estilos galeria TI*/


.galeria_ti{
    width: 100%;
    padding: 1%;
	position: relative;
	float: left;
}
 
.galeria_ti>div>div>form>div>div>span {
	display:none!important;
}    

/************* Estilos convocatoria***********************/
.contenedor_conv img{
	width:100%;
}
.contenido_conv{
	width:80%;
	margin: 5% 10%;
}
h2{
	text-align:center;
	position: absolute;
	margin-top: -65.5%;
	z-index: 50;
	color: #FFF;
	margin-left: 40%;
	font-weight:bold;
}
.intro{
	width:30%;
	text-align:center;
	position: absolute;
	margin-top: -62.5%;
	z-index: 50;
	color: #FFF;
	margin-left: 35%;
	font-weight:bold;
}

.datosimp_conv{
	width:80%;
	margin: 0 10%;
	display:block;
	clear:both;
	height: 220px;
}

.datosimp_conv div{
	width:40%;
	position:relative;
	float:left;
}

.datosimp_conv div:nth-child(3){
	width:20%;
}


.registro_conv h4 {
	text-align:center;
	margin-top:15px;
}

.registro_conv img{
	width:40%;
	margin-left:30%;
	margin-right:30%;
}

.registro_conv img:hover{
	width:45%;
	margin-left:27.5%;
	margin-right:27.5%;
}

.mecanica_conv{
	width:80%;
	height:auto;
	margin:4% 10%;
	display:inline-block;
	clear:both;
	border: 3px solid rgb(249, 249, 249);
} 

.mecanica_conv div:{
	text-align:center;
}

.mecanica_conv div div:first-child{
	text-align:center;
}


.mecanica_conv_0 div, .mecanica_conv_1 div, .mecanica_conv_2 div, .mecanica_conv_3 div, .mecanica_conv_4 div, .mecanica_conv_5 div{
	position:relative;
	float:left;
	padding-top: 15px;
}

.mecanica_conv_0 div:first-child,.mecanica_conv_1 div:first-child,.mecanica_conv_2 div:first-child, .mecanica_conv_3 div:first-child, .mecanica_conv_4 div:first-child, .mecanica_conv_5 div:first-child{
	width:40%;
}



.mecanica_conv_0 div:nth-child(2),.mecanica_conv_1 div:nth-child(2),.mecanica_conv_2 div:nth-child(2),.mecanica_conv_3 div:nth-child(2),.mecanica_conv_4 div:nth-child(2), .mecanica_conv_5 div:nth-child(2){
	width:60%;
}

.mecanica_conv_1 ul li{
	font-size:14px;
}

.mecanica_conv_0 div{
	background-color:rgb(249, 249, 249);
	height:70px;
}

.mecanica_conv_1 div{
	overflow-x:auto;
}

.mecanica_conv_2 div, .mecanica_conv_4 div {
	background-color:rgb(249, 249, 249);
	height:160px;
}

.mecanica_conv_2 div:nth-child(2), .mecanica_conv_4 div:nth-child(2){
	overflow-y: auto;
}

.contenido_premio_conv{
	width:80%;
	margin: 0 10%;
}

.contenido_premio_conv h4{
	text-align:center;
}

.contenido_premio_conv ul li{
	font-size:15px;
}

.lugares_premio{
	width: 60%;
	margin: 3% 20%;
	display:inline-block;
	clear:both;
}

.premio_rec{
	text-align: center;
}

.primer_lugar, .segundo_lugar, .tercer_lugar{
	width:33%;
	height:200px;
	float:left;
	position:relative;
}

.primer_lugar div p, .segundo_lugar div p, .tercer_lugar div p{
	text-align:center;
	color:#FFF;
	vertical-align:middle;
}

.primer_lugar div p span, .segundo_lugar div p span, .tercer_lugar div p span{
	font-weight: bold;
}

.segundo_lugar div{
	height:140px;
	background-color:rgb(98, 177, 208);
	margin-top: 40px;
}

.primer_lugar div{
	height:180px;	
	background-color:rgb(252, 170, 32);
}
.tercer_lugar div{
	height:100px;	
	background-color:rgb(252, 97, 31);
	margin-top: 80px;
	color: #FFF;
}

.fechas_conv{
	/*background-color: rgb(99, 218, 242);*/
	background-color: rgb(98, 177, 208);
	height:100%;
}

.contacto_conv{
	/*background-color: rgb(184, 240, 53);*/
	background-color: rgb(96, 205, 140);
	height:100%;
}

.fechas_conv h4, .contacto_conv h4{
	text-align:center;
	color: #FFF;
	font-weight:Bold;
}

.fechas_conv p, .contacto_conv p{
	text-align:center;
	color:#FFF;
}
/*
.primer_lugar, .segundo_lugar, .tercer_lugar{
	
}
 */   
</style>
<!-- Blog Section -->

<div class="contenedor_conv">
	<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/portada_convocatoria.jpg"> 
	<div class="contenido_conv">
	<h4>Meta</h4>
	<p>Diseñar el retail del futuro con tecnología existente (incluir proveedores) que genera mejor desempeño para la tiendita mexicana y visibilidad del comportamiento del consumidor dentro de la tienda (puede tomarse un Modelorama como inspiración).</p>
	</div>
	<div class="datosimp_conv">
		<div class="fechas_conv">
			<h4>Definición de tiempos</h4>
			<p>Fecha de publicación e inicio: 10 de febrero</p>
			<p>Fecha de cierre: 27 de febrero</p>
			<p>Fecha de evaluación: 1 y 2 de marzo</p>
			<p>Mención de finalistas: 3 de marzo</p>

		</div>
		<div class="contacto_conv">
			<h4>Contactos</h4>
			<p>Diana Aranda     diana.aranda@gmodelo.com.mx</p>
			<p>Mackenzie Banks  mackenzie.banks@gmodelo.com.mx</p> 

		</div>
		<div class="registro_conv">
			<a href="http://www.haler.com.mx/registro_convocatoria/"><img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/icono_registro.png"></a>
			<h4><a href="http://www.haler.com.mx/registro_convocatoria/">Registro</a></h4>
		</div>
	</div>
	<div class="mecanica_conv">
	<div class="mecanica_conv_0">
			<div><h4>Institución</h4></div>
			<div>
				<p>Grupo Modelo México</p>
			</div>
		</div>
		<div class="mecanica_conv_1">
			<div><h4>Mecánica del Reto</h4></div>
			<div>
				<p>Grupo Modelo busca que sus socios estratégicos crezcan y apoya a la economía familiar de las tiendas tradicionales. Incluir tecnología en las tiendas, y nuevas ideas que a nosotros nos permita conocer el comportamiento del consumidor y entender nuestras ventas es clave en este proyecto.</p>
				<p>Requisitos:</p>
				<ul>
					<li>Debes Registrarte en: <a>ti.haler.com.mx</a></li>
					<li>También es obligatorio registrarse en esta página, de lo contrario el registro no contará:  <a href="https://hacktheworld.ab-inbev.com/MexicoCity_2017#registration-form">https://hacktheworld.ab-inbev.com/MexicoCity_2017#registration-form</a></li>
				</ul>
			</div>
		</div>
		<div class="mecanica_conv_2">
			<div><h4>Justificación</h4></div>
			<div>
				<p>Uno de los principales socios estratégicos de Grupo Modelo son sus clientes retailers. Es por ello que estamos buscando ideas que puedan ayudar nuestros partners a evolucionar sus negocios, incrementar sus ingresos, generar compromiso con los consumidores, y aumentar nuestras ventas.</p>
			</div>
		</div>
		<div class="mecanica_conv_3">
			<div><h4>Elementos a evaluar</h4></div>
			<div><p>Originalidad, viabilidad para su implementación, impacto en el negocio de Grupo Modelo.</p></div>
		</div>
		<div class="mecanica_conv_4">
			<div><h4>Información Auxiliar</h4></div>
			<div>
				<p><a href="http://www.nielsen.com/content/dam/nielsenglobal/co/docs/Reports/2016/6%20Tendencias%20del%20Retail%20en%20Am%C3%A9rica%20Latina.pdf">http://www.nielsen.com/content/dam/nielsenglobal/co/docs/Reports/2016/6%20Tendencias%20del%20Retail%20en%20Am%C3%A9rica%20Latina.pdf</a></p>
				<p><a href="http://www.merca20.com/tendencias-sector-retail-2017/">http://www.merca20.com/tendencias-sector-retail-2017/</a></p>
			</div>
		</div>
		
	</div>
	<div class="contenido_premio_conv">
		<h4>Premio</h4>
		<p class="premio_rec">Reconocimiento Institucional además de:</p>
		<div class="lugares_premio">
			<div class="segundo_lugar"><div><p><span>Segundo Lugar</span>$ 10,000.00 pesos MXN.</p></div></div>
			<div class="primer_lugar"><div><p><span>Primer lugar</span>$ 20,000.00 pesos MXN.</p></div></div>
			<div class="tercer_lugar"><div><p><span>Tercer lugar</span>$ 5,000.00 pesos MXN.</p></div></div>
		</div>
		<h4>Entrega del premio</h4>
		<div>
			<p>Para recibir el premio, el equipo ganador deberá entregar el documento en extenso en no más de 30 diapositivas de power point, en donde describa a detalle la ejecución del proyecto y considere la retroalimentación de las mentorias y del jurado.
			</p>
			<p>Este documento deberá incluir lo siguiente: </p>
			<ul>
				<li>1.	Descripción de la idea</li>
				<li>2.	Equipo de trabajo</li>
				<li>3.	Modelo operativo</li>
				<li>4.	Esquema de implementación</li>
				<li>5.	Presupuesto para su ejecución</li>
				<li>6.	Cronograma de actividades</li>
				<li>7.	Información adicional (Especificaciones técnicas, fotos, videos, etc.)</li>
			</ul>
			<p>Así mismo, deberá firmar una carta de recepción del premio.</p>
		</div>
	</div>
</div>

<a name="Contacto" id="b"></a>
        <!--edición fondos-->
<?php 
$appointment_options=theme_setup_data();
$callout_setting = wp_parse_args(  get_option( 'appointment_options', array() ), $appointment_options );
if($callout_setting['home_call_out_area_enabled'] == 0 ) { 
 $imgURL = $callout_setting['callout_background'];
 if($imgURL != '') { ?>
<div class="callout-section" style="background-image:url('../wp-content/themes/appointment/images/im/Negro.png'); background-repeat: no-repeat; background-position: top left; background-attachment: fixed;">

<?php } 
else
{ ?> 
<div class="callout-section" style="background-color:#ccc;">
<?php } ?>
	<!--div class="overlay">
		<div class="container">
			<div class="row">	
				<div class="col-md-12">	
						
						<h1><!--?php echo $callout_setting['home_call_out_title'];?></h1>
						 <p><!--?php echo $callout_setting['home_call_out_description']; ?></p>
					
						<div><!--?php if ( function_exists( 'smuzform_render_form' ) ) { smuzform_render_form('204'); }?></div>
				</div>	
			</div>			
		
		</div>
			
	</div>	
</div--> 
<!-- /Callout Section -->
<div class="clearfix"></div>
<?php } ?>
<!--edición fondos-->
<style>
    #piebord > p{
        background-color: #fff!important;
        border-top: 5px #000 solid;
    }
</style>
<!-- /Blog Section with Sidebar -->
<?php get_footer(); ?>