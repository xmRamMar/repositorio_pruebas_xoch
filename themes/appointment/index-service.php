<style>

/*Cambios de estilo slide*/

#cycloneslider-4-1 {
	max-width: 100%!important;
	width: 100%!important;
}

#cycloneslider-4-1 img{
	width: 100%!important;
}

.cycloneslider-caption {
	margin-left: 15%;
}

.cycloneslider-caption div {
	color: white!important;
}

.cycloneslider-pager{
	width:80%!important;
	margin: 0 10%!important;
	bottom: 50%!important;
	height: 50px!important;	
}


.cycloneslider-pager span{
	float:left;
	border-radius: 2px!important;
	margin-left:7.5%!important;
	margin-right:7.5%!important;
	font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
	font-size: 18px!important;
	font-weight: 400!important;
	line-height:60px!important;
	list-style-type:none!important;
}

.cycloneslider-pager span:hover{
	opacity:0.5!important;		
}


.cycloneslider-pager span:nth-child(1){
	background-color: #000000!important;
}

.cycloneslider-pager span:nth-child(2){
	background-color: #cb3a3c!important;		
	text-indent: -12px;
}

.cycloneslider-pager span:nth-child(3){
	background-color: #ff9406!important;
	text-indent: -32px;
}

.cycloneslider-pager span:nth-child(4){
	background-color: #f2cf3c!important;
	text-indent: -32px;
}
.cycloneslider-pager span:nth-child(5){
	background-color: #acd624!important;
	text-indent: -58px;
}

.cycloneslider-pager span:nth-child(6){
	background-color: #80aa22!important;
	text-indent: -27px;
}

.cycloneslider-pager span:first-child:before{
	content:"TI";
	color: #000000;	
}


.cycloneslider-pager span:nth-child(2):before{
	content:"DoIn";
	color: #cb3a3c;
}

.cycloneslider-pager span:nth-child(3):before{
	content:"Academy";
	color: #ff9406;
}

.cycloneslider-pager span:nth-child(4):before{
	content:"Biomedic";
	color: #f2cf3c;
}
.cycloneslider-pager span:nth-child(5):before{
	content:"TheEngeeniers";
	color: #acd624;
}

.cycloneslider-pager span:nth-child(6):before{
	content:"Industry";
	color: #80aa22;
}


.cycloneslider-template-standard div[data-cycle-dynamic-height="off"] .cycloneslider-slide-image{
	height:auto!important;
}

.cycloneslider-caption-title{
	/*background-color:transparent!important;*/
}

.cycloneslider-caption-description {
	font-size:16px!important;
}

.cycloneslider-template-standard {
	margin-bottom:-17%!important;
}

.cycloneslider-template-standard .cycloneslider-caption{
	/*width:auto!important;*//*Roy*/
	/*background-color:transparent!important;*/
	color:black!important;
}

/* ********************************************************************
 * *************************** RESPONSIVE *************************** *
 ******************************************************************** */

@media only screen
and (min-width : 320px){

	.cycloneslider-pager{
		margin-left:35%!important;
		bottom:-3%!important;
		/**width: auto!important;**//*Roy*/
	}
	
	.cycloneslider-pager span::before{
		content:""!important;
	}
	
	.cycloneslider-pager span{
		margin:0 3px!important;
	}
	/*nuevos*/
	cycloneslider-home_slide-1 {
		height:250px;
	}
	
	.cycle-slideshow {
		height:250px!important;
	}
	
	/*.cycloneslider-template-standard .cycloneslider-slide img {
		height:100%;
	}*/
	.cycloneslider-caption{
		bottom:auto!important; /*0*/
		margin-top:0.2%!important; /*auto, no estaba*/
		margin-left:0!important; /*15%*/
		opacity:1!important;/*0.7*/
	}
	.cycloneslider-caption-description {
		height:200px!important;
	}
	
	.cycloneslider-pager {
		/*position:static!important; absolute*/
	}
}

@media only screen
and (min-width : 480px){

	.cycloneslider-pager{
		margin-left:35%!important;
		bottom:-25%!important;
		/**width: auto!important;**//*Roy*/
		/*position:absolute!important; absolute*/
	}
	
	.cycloneslider-pager span::before{
		content:""!important;
	}
	
	.cycloneslider-pager span{
		margin:0 3px!important;
	}
	
	.cycloneslider-caption{
		position:relative!important;
		margin-top: 0.1% !important;
	}
	.section-heading-title{
		margin: 18% auto 53px; /*0 auto 53px*/
	}
	
	.cycle-slideshow {
		height: 300px!important;
	}
	
}

@media only screen
and (min-width : 768px){

	.cycloneslider-pager{
		width:95%!important;/*width:80%!important;*/
		margin: 0 2.5%!important;/*margin: 0 10%!important;*/
		bottom: 50%!important;
		height: 50px!important;	
	}
	
	.cycloneslider-pager span:first-child::before{
		content:"TI"!important;
		color: #000000;	
	}


	.cycloneslider-pager span:nth-child(2)::before{
		content:"DoIn"!important;
		color: #cb3a3c;
	}

	.cycloneslider-pager span:nth-child(3)::before{
		content:"Academy"!important;
		color: #ff9406;
	}

	.cycloneslider-pager span:nth-child(4)::before{
		content:"Biomedic"!important;
		color: #f2cf3c;
	}
	.cycloneslider-pager span:nth-child(5)::before{
		content:"TheEngineers"!important;
		color: #acd624;
	}

	.cycloneslider-pager span:nth-child(6)::before{
		content:"Industry"!important;
		color: #80aa22;
	}
		
	.cycloneslider-pager span{
		margin-left:7%!important;/*margin-left:7.5%!important*/
		margin-right:7%!important;/*margin-right:7.5%!important*/
	}
	
	/*nuevos*/
	cycloneslider-home_slide-1 {
		height:400px;
	}
	
	.cycle-slideshow {
		height:400px!important;
	}
	
	.cycloneslider-caption{
		bottom:0!important; /*0*/
		margin-top:auto!important; /*auto, no estaba*/
		margin-left:0%!important; /*15%*/
		opacity:1!important;/*0.7*/
	}
	.cycloneslider-caption-description {
		height:auto!important;
	}
	
	.section-heading-title{
		margin: 0 auto 53px; /*0 auto 53px*/
	}
	
}

@media only screen
and (min-width : 1024px){
	/*nuevos*/
	cycloneslider-home_slide-1 {
		height:480px!important;
	}
	
	.cycle-slideshow {
		height:480px!important;
	}
	
	.cycloneslider-caption{
		"*width: auto!important;*//*Roy*/
		position:absolute!important;
		bottom:0!important; /*0*/
		margin-top:auto!important; /*auto, no estaba*/
		/*margin-left:15%!important;*/ /*15% comentado Roy*/
		opacity:0.7!important;/*0.7*/
		
	}
	
	.cycloneslider-pager {
		position:absolute!important;
		width:80%!important;/*width:80%!important;*/
		margin: 0 10%!important;/*margin: 0 10%!important;*/
	}
	
	.cycloneslider-pager span{
		margin-left:7.5%!important;/*margin-left:7.5%!important*/
		margin-right:7.5%!important;/*margin-right:7.5%!important*/
	}
	
	.cycloneslider-pager span{
		float:left;
		border-radius: 2px!important;
		margin-left:7.5%!important;
		margin-right:7.5%!important;
		font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
		font-size: 18px!important;
		font-weight: 400!important;
		line-height:60px!important;
		list-style-type:none!important;
	}
	
	/******************/
	
}

@media only screen
and (min-width : 1200px){
	.cycloneslider-pager{
		bottom: 35%!important;
	}
	
	.Service-section{
		margin-top:15%!important;
	}
}
</style>
<style>
    h3 {
     font-weight: 800;
    }
        .service-iconx {
        margin-bottom: 3%;
    }
    .service-iconx>img {
        display: flexbox;
        border-radius: 50%;
        padding: 3%;
        width: 20%;
        height: 20%;
        border: 3px solid #d0d0d0;
        background: #80aa22;
    }
    /**ici**/
	.row :nth-last-child(n) :hover .service-iconx img{ /*.service-area:hover .service-icon i !!!!VERIFICAR FUNCIONAMIENTO*/
         list-style: none;
        display: flexbox;
        border-radius: 50%;
        padding: 2%;
        border: 4px solid #677a1a;/*cambiar*/
    }

    .media {
        display: block;
    }
    table,tr,td {
        border: 0;
        background: #ffffff;
        width:100%; 
        border-color:  #ffffff!important;
        vertical-align: top;
        text-align: center;
    }
    
    .row .col-md-4:nth-child(1) {
        display: block !important;
        text-align: center;
}
    .row .col-md-4:nth-child(2) {
        display: block !important;
        text-align: center;
}
    .row .col-md-4:nth-child(3) {
        display: block !important;
        text-align: center;
}
</style>
<style>
/*Cambios de estilo slide*/

#cycloneslider-home_slide-1 {
	max-width: 100%!important;
	width: 100%!important;
}

#cycloneslider-home_slide-1 img{
	width: 100%!important;
}

.cycloneslider-caption {
	margin-left: 15%;
}

.cycloneslider-caption div {
	color: white!important;
}

.cycloneslider-pager{
	width:80%!important;
	margin: 0 10%!important;
	bottom: 50%!important;
	height: 50px!important;	
}


.cycloneslider-pager span{
	float:left;
	border-radius: 2px!important;
	margin-left:7.5%!important;
	margin-right:7.5%!important;
	font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
	font-size: 18px!important;
	font-weight: 400!important;
	line-height:60px!important;
	list-style-type:none!important;
}

.cycloneslider-pager span:hover{
	opacity:0.5!important;		
}


.cycloneslider-pager span:first-child {
	background-color: #000000!important;
}

.cycloneslider-pager span:nth-child(2){
	background-color: #cb3a3c!important;		
	text-indent: -12px;
}

.cycloneslider-pager span:nth-child(3){
	background-color: #ff9406!important;
	text-indent: -32px;
}

.cycloneslider-pager span:nth-child(4){
	background-color: #f2cf3c!important;
	text-indent: -32px;
}
.cycloneslider-pager span:nth-child(5){
	background-color: #acd624!important;
	text-indent: -58px;
}

.cycloneslider-pager span:nth-child(6){
	background-color: #80aa22!important;
	text-indent: -27px;
}

.cycloneslider-pager span:first-child::before{
	content:"TI";
	color: #000000;	
}


.cycloneslider-pager span:nth-child(2)::before{
	content:"DoIn";
	color: #cb3a3c;
}

.cycloneslider-pager span:nth-child(3)::before{
	content:"Academy";
	color: #ff9406;
}

.cycloneslider-pager span:nth-child(4)::before{
	content:"Biomedic";
	color: #f2cf3c;
}
.cycloneslider-pager span:nth-child(5)::before{
	content:"TheEngeeniers";
	color: #acd624;
}

.cycloneslider-pager span:nth-child(6)::before{
	content:"Industry";
	color: #80aa22;
}


.cycloneslider-template-standard div[data-cycle-dynamic-height="off"] .cycloneslider-slide-image{
	height:auto!important;
}

.cycloneslider-caption-title{
	/*background-color:transparent!important;*/
}

.cycloneslider-caption-description {
	font-size:16px!important;
}

.cycloneslider-template-standard {
	margin-bottom:-17%!important;
}

.cycloneslider-template-standard .cycloneslider-caption{
	width:auto!important;
	/*background-color:transparent!important;*/
	color:black!important;
}
/***RESPONSIVE AÑADIDO EL 12/12/2016***/

/* ********************************************************************
 * *************************** RESPONSIVE *************************** *
 ******************************************************************** */

@media only screen
and (min-width : 320px){

	.cycloneslider-pager{
		margin-left:35%!important;
		bottom:-3%!important;
		width: auto!important;
	}
	
	.cycloneslider-pager span::before{
		content:""!important;
	}
	
	.cycloneslider-pager span{
		margin:0 3px!important;
	}
	/*nuevos*/
	#cycloneslider-4-1 {
		height:250px;
	}
	
	.cycle-slideshow {
		height:250px!important;
	}
	
	/*.cycloneslider-template-standard .cycloneslider-slide img {
		height:100%;
	}*/
	.cycloneslider-caption{
		bottom:auto!important; /*0*/
		margin-top:0.2%!important; /*auto, no estaba*/
		margin-left:0!important; /*15%*/
		opacity:1!important;/*0.7*/
	}
	.cycloneslider-caption-description {
		height:200px!important;
	}
	
	.cycloneslider-pager {
		/*position:static!important; absolute*/
	}
}

@media only screen
and (min-width : 480px){

	.cycloneslider-pager{
		margin-left:35%!important;
		bottom:-25%!important;
		width: auto!important;
		/*position:absolute!important; absolute*/
	}
	
	.cycloneslider-pager span::before{
		content:""!important;
	}
	
	.cycloneslider-pager span{
		margin:0 3px!important;
	}
	
	.cycloneslider-caption{
		position:relative!important;
		margin-top: 0.1% !important;
	}
	.section-heading-title{
		margin: 18% auto 53px; /*0 auto 53px*/
	}
	
	.cycle-slideshow {
		height: 300px!important;
	}
	
}

@media only screen
and (min-width : 768px){

	.cycloneslider-pager{
		width:95%!important;/*width:80%!important;*/
		margin: 0 2.5%!important;/*margin: 0 10%!important;*/
		bottom: 50%!important;
		height: 50px!important;	
	}
	
	.cycloneslider-pager span:first-child::before{
		content:"TI"!important;
		color: #000000;	
	}


	.cycloneslider-pager span:nth-child(2)::before{
		content:"DoIn"!important;
		color: #cb3a3c;
	}

	.cycloneslider-pager span:nth-child(3)::before{
		content:"Academy"!important;
		color: #ff9406;
	}

	.cycloneslider-pager span:nth-child(4)::before{
		content:"Biomedic"!important;
		color: #f2cf3c;
	}
	.cycloneslider-pager span:nth-child(5)::before{
		content:"TheEngeeniers"!important;
		color: #acd624;
	}

	.cycloneslider-pager span:nth-child(6)::before{
		content:"Industry"!important;
		color: #80aa22;
	}
		
	.cycloneslider-pager span{
		margin-left:7%!important;/*margin-left:7.5%!important*/
		margin-right:7%!important;/*margin-right:7.5%!important*/
	}
	
	/*nuevos*/
	#cycloneslider-4-1 {
		height:400px;
	}
	
	.cycle-slideshow {
		height:400px!important;
	}
	
	.cycloneslider-caption{
		bottom:0!important; /*0*/
		margin-top:auto!important; /*auto, no estaba*/
		margin-left:0%!important; /*15%*/
		opacity:1!important;/*0.7*/
	}
	.cycloneslider-caption-description {
		height:auto!important;
	}
	
	.section-heading-title{
		margin: 0 auto 53px; /*0 auto 53px*/
	}
	
}

@media only screen
and (min-width : 1024px){
	/*nuevos*/
	#cycloneslider-4-1 {
		height:480px!important;
	}
	
	.cycle-slideshow {
		height:480px!important;
	}
	
	.cycloneslider-caption{
		width: auto!important;
		position:absolute!important;
		bottom:0!important; /*0*/
		margin-top:auto!important; /*auto, no estaba*/
		margin-left:15%!important; /*15%*/
		opacity:0.7!important;/*0.7*/
		
	}
	
	.cycloneslider-pager {
		position:absolute!important;
		width:80%!important;/*width:80%!important;*/
		margin: 0 10%!important;/*margin: 0 10%!important;*/
	}
	
	.cycloneslider-pager span{
		margin-left:7.5%!important;/*margin-left:7.5%!important*/
		margin-right:7.5%!important;/*margin-right:7.5%!important*/
	}
	
	.cycloneslider-pager span{
		float:left;
		border-radius: 2px!important;
		margin-left:7.5%!important;
		margin-right:7.5%!important;
		font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
		font-size: 18px!important;
		font-weight: 400!important;
		line-height:60px!important;
		list-style-type:none!important;
	}
	
	/******************/
	
}

@media only screen
and (min-width : 1200px){
	.cycloneslider-pager{
		bottom: 35%!important;
	}
	
	.Service-section{
		margin-top:15%!important;
	}
}
</style>
<?php 
$appointment_options=theme_setup_data(); 
$service_setting = wp_parse_args(  get_option( 'appointment_options', array() ), $appointment_options );
if($service_setting['service_section_enabled'] == 0 ) { ?>
<div class="row" style="margin-left: 0px !important; margin-right: 0px !important;">
<div class="Service-section">
	<div class="container">
		</br>
		<div class="row"></br>
			<div class="col-md-12">
			
				<div class="section-heading-title"></br>
					<a name="Nosotros" id="a"></a>
					<h1 style="color: #80aa22; font-weight: 600;">Acerca de nosotros</h1>
					<p>HALER TECHNOLOGICAL DEVELOPMENT S.A. de C.V., es una compañía que está encaminada a la investigación, desarrollo e implementación de tecnologías para cubrir las necesidades de cada uno de sus clientes.</br>Gracias a la Infraestructura Técnica, Tecnológica y de Recursos Humanos que conforman nuestro grupo de trabajo, somos capaces de dar soluciones integrales a todas sus necesidades Tecnológicas. </p>
				</div>
			</div>
		</div>
		
<!---->
        <div class="row">
            <div class="col-md-4">
                <div class="service-iconx">
                            <img src="./wp-content/themes/appointment/images/im/icons/idea1.png" width="100%"alt="i" >
                        </div>
                        <div class="media-body">
                            <h3>Misión</h3>
                        </div>
                        <div class="media-body">
                            <p>Ser una empresa que apoye a sus clientes al mejor desarrollo de sus productos, así como implementar nuevos sistemas de manufactura que incrementen la productividad, ganancias y calidad de sus productos mediante diseños que reduzcan tanto los costos como los tiempos de desarrollo e implementación.</p> 
                        </div>
            </div>
            
            <div class="col-md-4">
                <div class="service-iconx">
                    <img src="./wp-content/themes/appointment/images/im/icons/cohete1.png" width="100%"alt="i" >
                </div>
                        <div class="media-body">
                            <h3>Visión</h3>
                        </div>
                        <div class="media-body">
                            <p>Ser la empresa líder en desarrollos tecnológicos que además de ser referente en el diseño, desarrollo de soluciones y servicios con altos estándares de calidad, ofrezca una experiencia agradable al usuario final.</p>
                        </div>
            </div>

            <div class="col-md-4">
                <div class="service-iconx">
                    <img src="./wp-content/themes/appointment/images/im/icons/pieza1.png" width="100%"alt="i" >
                </div>
                        <div class="media-body">
                            <h3>Valores</h3>
                        </div>
                        <div class="media-body">
                            <p>Pasión</br>
                            Responsabilidad</br>
                            Calidad</br>
                            Compromiso</p>
                        </div>
            </div>
        </div>
	</div>
</div>
</div>
<!-- /HomePage Service Section -->
<?php } ?>