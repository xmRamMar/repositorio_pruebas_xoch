<?php
/*
Template Name: Blank Page Trial
*/ ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> >
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="/wp-content/themes/appointment/publish/publish/landing/assets/img/favicon.png" type="image/x-icon">
    <title>Ayuda Meet-medic</title>
    <link rel="stylesheet" href="/wp-content/themes/appointment/publish/publish/landing/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/wp-content/themes/appointment/publish/publish/landing/assets/css/pe.css">
    <link rel="stylesheet" href="/wp-content/themes/appointment/publish/publish/landing/assets/css/landing-page.min.css">
    <link rel="stylesheet" id="wpsm_faq-font-awesome-front-css" href="http://www.haler.com.mx/wp-content/plugins/faq-responsive/assets/css/font-awesome/css/font-awesome.min.css?ver=4.7.3" type="text/css" media="all">
    <link rel="stylesheet" id="wpsm_faq_bootstrap-front-css" href="http://www.haler.com.mx/wp-content/plugins/faq-responsive/assets/css/bootstrap-front.css?ver=4.7.3" type="text/css" media="all">
        <script type="text/javascript" src="http://www.haler.com.mx/wp-includes/js/jquery/jquery.js?ver=1.12.4"></script>
        <script type="text/javascript" src="http://www.haler.com.mx/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1"></script>
        <script type="text/javascript" src="http://www.haler.com.mx/wp-content/themes/appointment/js/bootstrap.min.js?ver=4.7.3"></script>
    
    <link href="/wp-content/themes/appointment/publish/publish/landing/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
    <style>
        .list-group-item {
            border:0px;
        }
    </style>
    </head>
<body class="landing-page">
    <div id="skrollr-body">
        <?php include 'header.html';?>
        <div class="gap slide-slogan" id="slide-slogan">
            <div class="block-wrapper inner-content" style="padding-bottom: 0px!important;">
                <div class="slogan">
                    <p class="slogan-title">
                        Ayuda <span style="font-family:'Open Sans';"class="title-word">Meet-Medic</span>
                    </p>
                    <div class="slogan-separator"></div>
                    <p class="slogan-description">
                        Este es un instrumento con el cual los usuarios podrán guiarse para aprovechar las funcionalidades que ofrece Meet-Medic. Hemos separado las secciones de ayuda, por tipo de usuario, es decir, por: administración de la clínica, médico y pacientes; y adicionalmente, una sección en la que se abordan generalidades del sistema.
                    </p>
                    <div class="slogan-separator"></div>
                </div>
            </div>
        </div>
        <a name="Generalidades" id="b"></a>
        <div class="gap slide-slogan" >
            <div class="block-wrapper inner-content">
                <div class="slogan" style="padding-bottom: 0px; padding-top: 0px;">
                    <p class="slogan-title">
                        <br>Registro y organización de las páginas</p>
                </div>
            </div>
        </div>
        <div style="width: 90%; margin-left: 5%;margin-right: 5%; display: inline-block;">
            <?php   echo do_shortcode("[WPSM_FAQ id=340]"); ?>
        </div></br>
        <a name="Medicos" id="c"></a>
        <div class="gap slide-slogan" >
            <div class="block-wrapper inner-content">
                <div class="slogan" style="padding-bottom: 0px; padding-top: 0px;">
                    <p class="slogan-title">
                        <br>Médicos</p>
                </div>
            </div>
        </div>
        <div style="width: 90%; margin-left: 5%;margin-right: 5%; display: inline-block;">
            <?php   echo do_shortcode("[WPSM_FAQ id=356]"); ?>
        </div></br>
        <a name="UnidadesAdministrativas" id="d"></a>
        <div class="gap slide-slogan" >
            <div class="block-wrapper inner-content">
                <div class="slogan" style="padding-bottom: 0px; padding-top: 0px;">
                    <p class="slogan-title">
                        <br>Clínicas, Hospitales, Consultorios particulares (Unidades Administrativas)</p>
                </div>
            </div>
        </div>
        <div style="width: 90%; margin-left: 5%;margin-right: 5%; display: inline-block;">
            <?php   echo do_shortcode("[WPSM_FAQ id=357]"); ?>
        </div></br>
        <?php include 'footer.html';?>
    </div>
</body>
    
</html>