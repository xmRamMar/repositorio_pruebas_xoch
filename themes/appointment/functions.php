<?php
/**Theme Name	: Appointment
 * Theme Core Functions and Codes
*/
	/**Includes reqired resources here**/
	define('WEBRITI_TEMPLATE_DIR_URI', get_template_directory_uri());
    define('WEBRITI_TEMPLATE_DIR' , get_template_directory());
    define('WEBRITI_THEME_FUNCTIONS_PATH' , WEBRITI_TEMPLATE_DIR.'/functions');
	require( WEBRITI_THEME_FUNCTIONS_PATH .'/scripts/script.php');
    require( WEBRITI_THEME_FUNCTIONS_PATH .'/menu/default_menu_walker.php');
    require( WEBRITI_THEME_FUNCTIONS_PATH .'/menu/appoinment_nav_walker.php');
    require( WEBRITI_THEME_FUNCTIONS_PATH .'/widgets/sidebars.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH .'/widgets/appointment_info_widget.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/template-tag.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/breadcrumbs/breadcrumbs.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/font/font.php');
	//Customizer
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_theme_style.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer-callout.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer-slider.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer-copyright.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer-header.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer-news.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer-service.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer-pro.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer-project.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer-testimonial.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer-client.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer-footer-callout.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer-template.php');
	
	// Appointment Info Page
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/appointment-info/welcome-screen.php');
	
	// Custom Category control 
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/custom-controls/select/category-dropdown-custom-control.php');
	/* Theme Setup Function */
	add_action( 'after_setup_theme', 'appointment_setup' );
	
	function appointment_setup()
	{	
	// Load text domain for translation-ready
    load_theme_textdomain( 'appointment', WEBRITI_THEME_FUNCTIONS_PATH . '/lang' );

	$header_args = array(
				 'flex-height' => true,
				 'height' => 200,
				 'flex-width' => true,
				 'width' => 1600,
				 'admin-head-callback' => 'mytheme_admin_header_style',
				 );
				 
				 add_theme_support( 'custom-header', $header_args );
    add_theme_support( 'post-thumbnails' ); //supports featured image
	// Register primary menu 
    register_nav_menu( 'primary', __( 'Primary Menu', 'appointment' ) );
	register_nav_menu( 'navegacion-categoriasProd', __( 'Menu Productos', 'appointment' ) );
	
	//Add Theme Support Title Tag
	add_theme_support( "title-tag" );
	
	// Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );
	// Set the content_width with 900
    if ( ! isset( $content_width ) ) $content_width = 900;
	require_once('theme_setup_data.php');
	}
// set appoinment page title       
function appointment_title( $title, $sep )
{	
    global $paged, $page;
		
	if ( is_feed() )
        return $title;
		// Add the site name.
		$title .= get_bloginfo( 'name' );
		// Add the site description for the home/front page.
		$site_description = get_bloginfo( 'description' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			$title = "$title $sep $site_description";
		// Add a page number if necessary.
		if ( $paged >= 2 || $page >= 2 )
			$title = "$title $sep " . sprintf( __( 'Page', 'appointment' ), max( $paged, $page ) );
		return $title;
}	
add_filter( 'wp_title', 'appointment_title', 10,2 );

add_filter('get_avatar','appointment_add_gravatar_class');

function appointment_add_gravatar_class($class) {
    $class = str_replace("class='avatar", "class='img-responsive img-circle", $class);
    return $class;
}
function appointment_add_to_author_profile( $contactmethods ) {
		$contactmethods['facebook_profile'] = __('Facebook Profile URL','appointment');
		$contactmethods['twitter_profile'] = __('Twitter Profile URL','appointment');
		$contactmethods['linkedin_profile'] = __('Linkedin Profile URL','appointment');
		$contactmethods['google_profile'] = __('Google Profile URL','appointment');
		return $contactmethods;
		}
		add_filter( 'user_contactmethods', 'appointment_add_to_author_profile', 10, 1);
	
	
	    add_filter('get_the_excerpt','appointment_post_slider_excerpt');
	    function appointment_post_slider_excerpt($output){
		$output = strip_tags(preg_replace(" (\[.*?\])",'',$output));
		$output = strip_shortcodes($output);		
		$original_len = strlen($output);
		$output = substr($output, 0, 155);		
		$len=strlen($output);	 
		if($original_len>155) {
		$output = $output;
		return  '<div class="slide-text-bg2">' .'<span>'.$output.'</span>'.'</div>'.
	                       '<div class="slide-btn-area-sm"><a href="' . get_permalink() . '" class="slide-btn-sm">'.__("
						   Read more","appointment").'</a></div>';
		}
		else
		{ return '<div class="slide-text-bg2">' .'<span>'.$output.'</span>'.'</div>'; }   
        }
						
	function get_home_blog_excerpt()
	{
		global $post;
		$excerpt = get_the_content();
		$excerpt = strip_tags(preg_replace(" (\[.*?\])",'',$excerpt));
		$excerpt = strip_shortcodes($excerpt);		
		$original_len = strlen($excerpt);
		$excerpt = substr($excerpt, 0, 145);		
		$len=strlen($excerpt);	 
		if($original_len>275) {
		$excerpt = $excerpt;
		return $excerpt . '<div class="blog-btn-area-sm"><a href="' . get_permalink() . '" class="blog-btn-sm">Read More</a></div>';
		}
		else
		{ return $excerpt; }
	}
	
	remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
	remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
	add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
	add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

	add_action( 'after_setup_theme', 'woocommerce_support' );
	function woocommerce_support() {
   		add_theme_support( 'woocommerce' );
	}

	add_action('woocommerce_after_order_notes', 'my_custom_checkout_field');
	
 
	function my_custom_checkout_field( $checkout ) {
	echo '</br><input class="ckb_facturar" type="checkbox" value="facturar" name="facturar" onclick="mostrar()"><label class="lbl_facturar">Facturar</label></br>';
	echo '<div id="my_custom_checkout_field" class="cont_facturacion"><h3>'.__('Información para facturación').'</h3>';
	 
	woocommerce_form_field( 'rfc', array(
	'type' => 'text',
	'class' => array('my-field-class form-row-wide'),
	'label' => __('RFC'),
	'placeholder' => __('Escriba su RFC aquí'),
	'required' => true,
	), $checkout->get_value( 'rfc' ));
	
	woocommerce_form_field( 'razon_social', array(
	'type' => 'text',
	'class' => array('my-field-class form-row-wide'),
	'label' => __('Razón social'),
	'placeholder' => __('Razón social'),
	'required' => true,
	), $checkout->get_value( 'razonSocial' ));
	
	woocommerce_form_field( 'domicilio_fiscal', array(
	'type' => 'text',
	'class' => array('my-field-class form-row-wide'),
	'label' => __('Domicilio Fiscal'),
	'placeholder' => __('Domicilio Fiscal'),
	'required' => true,
	), $checkout->get_value( 'domFiscal' ));
	
	 
	echo '</div>';

echo '</div>';
	woocommerce_form_field( 'terminos', array(
	'type' => 'checkbox',
	'class' => array('my-field-class form-row-wide'),
	'label' => __('He leído y acepto los términos y condiciones'),
	'required' => true,
	), $checkout->get_value( 'terminos' ));	
	
	echo '<a href="http://www.haler.com.mx/terminos-condicionesshop/" target="_blank">términos y condiciones</a>';


	}
	
	
 
	function my_custom_checkout_field_process() {
	global $woocommerce;
		// Comprobar, solo en caso de que se haya seleccionado el checkbox, si el campo ha sido completado, en caso contrario agregar un error.
		if (isset($_POST['facturar'])){
			if (!$_POST['rfc'] || !$_POST['razon_social'] || !$_POST['domicilio_fiscal'])
				$woocommerce->add_error( __('Por favor introduce tus datos para facturación, gracias.') );
				//$woocommerce->add_notice( __('Por favor introduce tus datos para facturación, gracias.') );
			if (!$_POST['terminos'])
				$woocommerce->add_error( __('Por favor lee y acepta los términos y condiciones.') );
		}	
		else{
			if (!$_POST['terminos'])
				$woocommerce->add_error( __('Por favor lee y acepta los términos y condiciones.') );
		}
	}
	add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');
	
	
	
	function my_custom_checkout_field_update_order_meta( $order_id ) {
	if ($_POST['facturar']) update_post_meta( $order_id, 'Facturar', esc_attr($_POST['facturar']));
	if ($_POST['rfc']) update_post_meta( $order_id, 'RFC', esc_attr($_POST['rfc']));
	if ($_POST['razon_social']) update_post_meta( $order_id, 'Razón Social', esc_attr($_POST['razon_social']));
	if ($_POST['domicilio_fiscal']) update_post_meta( $order_id, 'Domicilio Fiscal', esc_attr($_POST['domicilio_fiscal']));
	}
	add_action('woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta');

	function my_theme_wrapper_start() {
	  echo '<section id="shop_content">';
	}

	function my_theme_wrapper_end() {
	  echo '</section>';
	}
?>