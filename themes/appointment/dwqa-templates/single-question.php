<?php
/**
 * The template for displaying all single questions
 *
 * @package DW Question & Answer
 * @since DW Question & Answer 1.4.3
 */
// global $wp_query; print_r( $wp_query );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> >
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="../../wp-content/themes/appointment/publish/publish/landing/assets/img/favicon.png" type="image/x-icon">
    <title>Foro Meet-Medic</title>
    <link rel="stylesheet" href="../../wp-content/themes/appointment/publish/publish/landing/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../wp-content/themes/appointment/style.css">
    <link rel="stylesheet" href="../../wp-content/themes/appointment/publish/publish/landing/assets/css/pe.css">
	<link rel="stylesheet" href="../../wp-content/themes/appointment/dwqa-templates/assets/css/style.css">
    <link rel="stylesheet" href="../../wp-content/themes/appointment/publish/publish/landing/assets/css/landing-page.min.css">
    <link rel="stylesheet" id="wpsm_faq-font-awesome-front-css" href="http://www.haler.com.mx/wp-content/plugins/faq-responsive/assets/css/font-awesome/css/font-awesome.min.css?ver=4.7.3" type="text/css" media="all">
    <link rel="stylesheet" id="wpsm_faq_bootstrap-front-css" href="http://www.haler.com.mx/wp-content/plugins/faq-responsive/assets/css/bootstrap-front.css?ver=4.7.3" type="text/css" media="all">
        <script type="text/javascript" src="http://www.haler.com.mx/wp-includes/js/jquery/jquery.js?ver=1.12.4"></script>
        <script type="text/javascript" src="http://www.haler.com.mx/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1"></script>
        <script type="text/javascript" src="http://www.haler.com.mx/wp-content/themes/appointment/js/bootstrap.min.js?ver=4.7.3"></script>
		<script type="text/javascript" src="http://www.haler.com.mx/wp-content/themes/appointment/dwqa-templates/assets/js/dwqa-single-question.js"></script>
    
    <link href="../../wp-content/themes/appointment/publish/publish/landing/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
    <base target="_parent" />
    <style>
        .landing-page .navbar-default .navbar-nav > li > a {
            color: #777;
        }
        
        .list-group-item {
            border:0px;
        }
		
		.contenedor_principal{
			position:relative;
		}
		
		.page-title-section {
			display:none;
		}
		
		.dwqa-breadcrumbs a{
			pointer-events: none;
		}

        .dwqa-question-vote, .dwqa-answer-vote, .dwqa-pick-best-answer {
            display: none!important;
        }

        .dwqa-question-item, .dwqa-answer-item {
            padding: 0;
        }
        
        
        
		
    </style>
    </head>
<body class="landing-page">
    <div id="skrollr-body">
		<div class="contenedor_principal content">
			<div class="dwqa-single-question">
			<?php if ( have_posts() ) : ?>
				<?php do_action( 'dwqa_before_single_question' ) ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php if ( !dwqa_is_edit() ) : ?>
						<?php dwqa_load_template( 'content', 'single-question' ) ?>
					<?php else : ?>
						<?php dwqa_load_template( 'content', 'edit' ) ?>
					<?php endif; ?>
				<?php endwhile; ?>
				<?php do_action( 'dwqa_after_single_question' ) ?>
			<?php endif; ?>
			</div>
		</div>
    </div>
</body>
    
</html>




